/** @format */

module.exports = {
  env: {
    browser: true,
    jest: true,
  },
  extends: ["airbnb-typescript-prettier", "react-app"],
  plugins: ["react-hooks"],
  rules: {
    "react-hooks/rules-of-hooks": "error",
    "react/jsx-props-no-spreading": "off",
    "react-hooks/exhaustive-deps": [
      "warn",
      {
        additionalHooks: "useRecoilCallback",
      },
    ],
  },
};
