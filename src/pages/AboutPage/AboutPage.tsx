import { Checkbox } from "antd";
import React from "react";
import Calendar from "../../components/Calendar";
import { DatePicker, TimePicker } from "../../components/DatePicker";
import useUser from "../../services/user-hook";

const AboutPage: React.FC = () => {
  const { userInfo } = useUser();
  return (
    <div>
      About
      <div>{JSON.stringify(userInfo, null, 2)}</div>
      <DatePicker />
      <TimePicker />
      <Calendar />
      <Checkbox />
    </div>
  );
};

export default AboutPage;
