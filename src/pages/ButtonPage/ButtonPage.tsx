import React from "react";
import styled from "styled-components";
import { Button } from "../../components/Button/Button";
import Radio from "../../components/Radio";
import Checkbox from "../../components/Checkbox";
import PageWrapper from "../../components/layout/PageWrapper";
import { Typo } from "../../components/Typo";

const Container = styled.div`
  display: grid;
  grid-gap: 10px;
`;

const ButtonPage: React.FC = () => {
  return (
    <PageWrapper>
      <Container>
        <Typo variant="h1" color="default">
          Button
        </Typo>
        <div>
          <Button>default</Button>
          <Button dense>default</Button>
          <Button variant="outlined" type="button">
            outline default
          </Button>
        </div>

        <div>
          <Button color="primary">primary</Button>
          <Button color="primary" dense>
            primary
          </Button>
          <Button color="primary" variant="outlined" type="button">
            outline primary
          </Button>
        </div>

        <div>
          <Button color="error">error</Button>
          <Button color="error" dense>
            error
          </Button>
          <Button color="error" variant="outlined" type="button">
            outline error
          </Button>
        </div>

        <Typo variant="h1" color="default">
          Radio
        </Typo>
        <Radio
          name="gender"
          label="Female"
          value="female"
          description="description"
        />
        <Radio
          name="gender"
          label="Male"
          value="Male"
          description="description"
        />
        <Radio
          name="gender"
          label="Other"
          value="Other"
          description="description"
        />
        <Radio name="gender" label="Disabled" description="disabled" disabled />

        <Typo variant="h1" color="default">
          Checkbox
        </Typo>
        <Checkbox
          name="staff"
          label="John"
          value="John"
          description="description"
        />
        <Checkbox
          name="staff"
          label="Sarah"
          value="Sarah"
          description="description"
        />
        <Checkbox
          name="staff"
          label="David"
          value="David"
          description="description"
        />
        <Checkbox
          name="staff"
          label="Disabled"
          description="disabled"
          disabled
        />
      </Container>
    </PageWrapper>
  );
};

export default ButtonPage;
