import React from "react";
import styled from "styled-components";
import PageWrapper from "../../components/layout/PageWrapper";
import { Typo } from "../../components/Typo";
import Tag from "../../components/Tag";
import BulbIcon from "../../components/Icon/icons/BulbIcon";

const Container = styled.div`
  display: grid;
  grid-gap: 10px;
`;

const TagPage: React.FC = () => {
  return (
    <PageWrapper>
      <Container>
        <Typo variant="h1" color="default">
          Tag
        </Typo>
        <div
          style={{ backgroundColor: "#fff", padding: "10px", display: "flex" }}
        >
          <Tag text="default" />
          <Tag showClose showBorder={false} text="default" />
          <Tag icon={<BulbIcon color="#333" />} text="default" />
          <Tag icon={<BulbIcon color="#333" />} showClose text="default" />
        </div>

        <div
          style={{ backgroundColor: "#fff", padding: "10px", display: "flex" }}
        >
          <Tag text="Positive" color="success" />
          <Tag showClose showBorder={false} text="Positive" color="success" />
          <Tag
            icon={<BulbIcon color="#333" />}
            text="Positive"
            color="success"
          />
          <Tag
            icon={<BulbIcon color="#333" />}
            showClose
            text="Positive"
            color="success"
          />
        </div>

        <div
          style={{ backgroundColor: "#fff", padding: "10px", display: "flex" }}
        >
          <Tag
            text="Volcano"
            color="#FF7A45"
            backgroundColor="#FFF2EC"
            borderColor="#FF7A45"
          />
          <Tag
            showClose
            text="Volcano"
            color="#FF7A45"
            backgroundColor="#FFF2EC"
            borderColor="#FF7A45"
          />
          <Tag
            icon={<BulbIcon color="#FF7A45" />}
            text="Volcano"
            color="#FF7A45"
            backgroundColor="#FFF2EC"
            borderColor="#FF7A45"
          />
          <Tag
            icon={<BulbIcon color="#FF7A45" />}
            showClose
            text="Volcano"
            color="#FF7A45"
            backgroundColor="#FFF2EC"
            borderColor="#FF7A45"
          />
        </div>
      </Container>
    </PageWrapper>
  );
};

export default TagPage;
