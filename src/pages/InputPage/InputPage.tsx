import React, { useState } from "react";
import styled from "styled-components";
import { Button } from "antd";
import { useForm } from "antd/lib/form/Form";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import PageWrapper from "../../components/layout/PageWrapper";
import { DatePicker, RangePicker } from "../../components/DatePicker";
import {
  Input,
  Form,
  Label,
  InputNumber,
  Textarea,
  FormItem,
  Upload,
} from "../../components/Input";
import { IUser } from "../../services/user.types";
import { Typo } from "../../components/Typo";

const Container = styled.div`
  display: grid;
  grid-gap: 10px;
  max-width: 350px;
`;

const InputPage: React.FC = () => {
  const [submitting, setSubmitting] = useState(false);
  const [form] = useForm();
  const [formUpload] = useForm();
  const onFinish = async (formdata: IUser) => {
    setSubmitting(true);
    setSubmitting(false);
  };

  // interface IUser {
  //   file: any;
  // }
  const onFinishUpload = async (formdata: any) => {
    console.log("PARNN,", formdata);
  };

  return (
    <PageWrapper>
      <Container>
        <div>
          Upload
          <br />
          <Form form={formUpload} name="upload" onFinish={onFinishUpload}>
            <Upload
              formItem={{
                name: "upload",
                rules: [{ required: true, message: "Please add file!" }],
              }}
              label={{
                text: "upload",
                labelHint: "upload",
                labelAlign: "left",
                showCount: true,
              }}
              helperText={{
                text: "Upload",
              }}
              align="left"
            />
            <FormItem padding="12px 0 0 0">
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </FormItem>
          </Form>
        </div>

        <div>
          DatePicker
          <br />
          <Label labelHint="test" labelIconColor="#838E9A" text="Select Date" />
          <DatePicker />
        </div>
        <div>
          DatePicker with custom format
          <br />
          <DatePicker format="DD/MM/YYYY" />
        </div>
        <div>
          RangePicker
          <br />
          <RangePicker />
        </div>
        <div>
          RangePicker with custom placeholder
          <br />
          <RangePicker placeholder={["วันเริ่มต้น", "วันสิ้นสุด"]} />
        </div>

        <div>
          Form and Input
          <br />
          <Form
            form={form}
            name="basic"
            initialValues={{ remember: true, prefix_amountselect: "A" }}
            onFinish={onFinish}
          >
            <Input
              formItem={{
                name: "username",
                rules: [
                  { required: true, message: "Please input your username!" },
                ],
              }}
              label={{
                text: "parn",
                labelIconColor: "red",
                showCount: true,
              }}
              helperText={{
                text: "parnn test ja",
                padding: "4px 0 4px 0",
                align: "left",
              }}
              texttype="text"
              suffix={<UserOutlined className="site-form-item-icon" />}
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
              maxLength={100}
            />

            <Input
              label={{
                size: "14px",
                lineHeight: "22px",
                color: "#505B67",
                showCount: true,
              }}
              texttype="text"
              suffix={<UserOutlined className="site-form-item-icon" />}
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
              maxLength={100}
            />

            <>
              <Input
                formItem={{
                  name: "amount",
                  rules: [
                    { required: true, message: "Please input your username!" },
                  ],
                }}
                label={{
                  text: "number",
                }}
                helperText={{
                  text: "parnn test ja",
                  align: "right",
                }}
                texttype="number"
                suffix={<Typo align="right">Unit</Typo>}
                placeholder="Username"
                align="right"
                formatter
                allowClear
              />

              <InputNumber
                label={{
                  text: "number2",
                  labelAlign: "right",
                }}
                placeholder="Number"
                inputAlign="right"
              />
            </>

            <Input
              formItem={{
                name: "amountselect",
                rules: [
                  { required: true, message: "Please input your username!" },
                ],
              }}
              label={{
                text: "text with select",
                labelHint: "texttt hint",
                labelAlign: "left",
              }}
              helperText={{
                text: "parnn test ja",
                padding: "4px 0 4px 0",
                align: "left",
              }}
              texttype="text"
              suffix={<Typo align="right">Unit</Typo>}
              placeholder="SELECT"
              align="left"
              select={{
                defaultValue: "PARN",
                options: [
                  { value: "PARN", text: "PARN" },
                  { value: "A", text: "B" },
                ],
              }}
            />

            <Textarea
              formItem={{
                name: "textarea",
                rules: [
                  { required: true, message: "Please input your username!" },
                ],
              }}
              label={{
                text: "textarea",
                labelHint: "textarea",
                labelAlign: "left",
                showCount: true,
              }}
              helperText={{
                text: "TEXT ARAA",
              }}
              placeholder="textarea"
              align="left"
              maxLength={100}
              height="100px"
              expanded
            />

            <Input
              formItem={{
                name: "password",
                rules: [
                  { required: true, message: "Please input your password!" },
                ],
                padding: "12px 0 0 0",
              }}
              texttype="password"
              prefix={<LockOutlined className="site-form-item-icon" />}
              placeholder="Password"
            />

            <FormItem padding="12px 0 0 0">
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </FormItem>

            <FormItem align="center" padding="12px 0 0 0">
              <Button
                type="default"
                danger
                htmlType="button"
                disabled={submitting}
              >
                Alert
              </Button>
            </FormItem>
          </Form>
        </div>
      </Container>
    </PageWrapper>
  );
};

export default InputPage;
