import React, { useState } from "react";
import styled from "styled-components";
import PageWrapper from "../../components/layout/PageWrapper";
import { Typo } from "../../components/Typo";
import Modal from "../../components/Modal";
import { Button } from "../../components/Button/Button";

const Container = styled.div`
  display: grid;
  grid-gap: 10px;
`;

const ModalPage: React.FC = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const handleOnClose = () => {
    setModalOpen(!modalOpen);
  };

  const [modalOpenI, setModalOpenI] = useState(false);
  const handleOnCloseI = () => {
    setModalOpenI(!modalOpenI);
  };

  const [modalOpenS, setModalOpenS] = useState(false);
  const handleOnCloseS = () => {
    setModalOpenS(!modalOpenS);
  };

  const [modalOpenW, setModalOpenW] = useState(false);
  const handleOnCloseW = () => {
    setModalOpenW(!modalOpenW);
  };

  const [modalOpenE, setModalOpenE] = useState(false);
  const handleOnCloseE = () => {
    setModalOpenE(!modalOpenE);
  };

  return (
    <PageWrapper>
      <Container>
        <Typo variant="h1" color="default">
          Tag
        </Typo>
        <div>
          <Button onClick={handleOnClose}>Modal Default</Button>
          <Modal
            text="modal detail"
            open={modalOpen}
            title="Modal Title"
            onClose={handleOnClose}
            onCancel={handleOnClose}
            onSubmit={handleOnClose}
          />
        </div>
        <div>
          <Button onClick={handleOnCloseI}>Modal Info</Button>
          <Modal
            text="modal detail info"
            open={modalOpenI}
            title="Modal Title Info"
            type="info"
            onClose={handleOnCloseI}
            onCancel={handleOnCloseI}
            onSubmit={handleOnCloseI}
          />
        </div>
        <div>
          <Button onClick={handleOnCloseS}>Modal Success</Button>
          <Modal
            transition="right"
            text="modal detail success"
            open={modalOpenS}
            title="Modal Title Success"
            type="success"
            onClose={handleOnCloseS}
            onCancel={handleOnCloseS}
            onSubmit={handleOnCloseS}
          />
        </div>
        <div>
          <Button onClick={handleOnCloseW}>Modal Warning</Button>
          <Modal
            transition="left"
            text="modal detail warning"
            open={modalOpenW}
            title="Modal Title Warning"
            type="warning"
            onClose={handleOnCloseW}
            onCancel={handleOnCloseW}
            onSubmit={handleOnCloseW}
          />
        </div>
        <div>
          <Button onClick={handleOnCloseE}>Modal Error</Button>
          <Modal
            transition="down"
            text="modal detail error"
            open={modalOpenE}
            title="Modal Title Error"
            type="error"
            onClose={handleOnCloseE}
            onCancel={handleOnCloseE}
            onSubmit={handleOnCloseE}
          />
        </div>
      </Container>
    </PageWrapper>
  );
};

export default ModalPage;
