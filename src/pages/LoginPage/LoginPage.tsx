import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { useForm } from "antd/lib/form/Form";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useLocation } from "react-router";
import { useSetRecoilState } from "recoil";
import styled from "styled-components";
import useAuth from "../../services/auth-hook";
import { IUser } from "../../services/user.types";
import alertState from "../../components/Alert/alertState";
import PageWrapper from "../../components/layout/PageWrapper";
import { Input, Form, FormItem } from "../../components/Input";

const Root = styled.div`
  max-width: 444px;
  margin: 128px auto;
  text-align: center;
  display: flex;
  flex-direction: column;
`;

const LoginButton = styled(Button)`
  width: 100%;
`;
const H1 = styled.h1`
  font-size: 24px;
`;

interface LocationState {
  from: {
    pathname: string;
  };
}

const LoginPage: React.FC = (props) => {
  const { t } = useTranslation();
  const history = useHistory();
  const location = useLocation<LocationState>();
  const { from } = location.state || { from: { pathname: "/" } };

  const { login } = useAuth();
  const [form] = useForm();
  const [submitting, setSubmitting] = useState(false);

  const setAlertState = useSetRecoilState(alertState);
  const handleClick = (): void => {
    setAlertState({
      message: "Alert!",
      type: "error",
    });
  };
  const onFinish = async (formdata: IUser) => {
    setSubmitting(true);
    await login({ username: formdata.username, password: formdata.password });
    setSubmitting(false);

    history.replace(from);
  };

  return (
    <PageWrapper hasSider={false} {...props}>
      <Root>
        <H1>{t("Login")}</H1>
        <Form
          form={form}
          name="basic"
          initialValues={{ remember: true, prefix_amountselect: "A" }}
          onFinish={onFinish}
        >
          <Input
            formItem={{
              name: "username",
              rules: [
                { required: true, message: "Please input your username!" },
              ],
            }}
            label={{
              text: "parn",
              labelIconColor: "red",
              showCount: true,
            }}
            helperText={{
              text: "parnn test ja",
              padding: "4px 0 4px 0",
              align: "left",
            }}
            texttype="text"
            suffix={<UserOutlined className="site-form-item-icon" />}
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Username"
            maxLength={100}
          />

          <Input
            formItem={{
              name: "password",
              rules: [
                { required: true, message: "Please input your password!" },
              ],
              padding: "12px 0 0 0",
            }}
            texttype="password"
            prefix={<LockOutlined className="site-form-item-icon" />}
            placeholder="Password"
          />

          <FormItem padding="12px 0 0 0">
            <LoginButton type="primary" htmlType="submit">
              Submit
            </LoginButton>
          </FormItem>

          <FormItem align="center" padding="12px 0 0 0">
            <Button
              type="default"
              danger
              htmlType="button"
              onClick={handleClick}
              disabled={submitting}
            >
              Alert
            </Button>
          </FormItem>
        </Form>
      </Root>
    </PageWrapper>
  );
};

export default LoginPage;
