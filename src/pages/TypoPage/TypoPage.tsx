import React from "react";
import styled from "styled-components";
import { Typo } from "../../components/Typo";
import PageWrapper from "../../components/layout/PageWrapper";

const Container = styled.div`
  display: grid;
  grid-gap: 10px;
`;

const ButtonPage: React.FC = () => {
  return (
    <PageWrapper>
      <Container>
        <div>
          Variant : undefined (props/css cannot used un this case)
          <br />
          <Typo>Undefined</Typo>
        </div>

        <div>
          Variant : h1
          <br />
          <Typo variant="h1">H1</Typo>
        </div>

        <div>
          Variant : h2
          <br />
          <Typo variant="h2" color="default">
            H1
          </Typo>
        </div>

        <div>
          Variant : h3
          <br />
          <Typo variant="h3" color="error">
            H1
          </Typo>
        </div>

        <div>
          Variant : h4
          <br />
          <Typo variant="h4" color="white">
            H4
          </Typo>
        </div>

        <div>
          Variant : h5
          <br />
          <Typo variant="h5" color="#3d3d3d">
            H5
          </Typo>
        </div>

        <div>
          Variant : h6
          <br />
          <Typo variant="h6">H6</Typo>
        </div>

        <div>
          Variant : p
          <br />
          <Typo variant="p" color="tessst">
            P
          </Typo>
        </div>

        <div>
          Variant : div
          <br />
          <Typo variant="div">div</Typo>
        </div>

        <div>
          Variant : span
          <br />
          <Typo variant="span">span</Typo>
        </div>
      </Container>
    </PageWrapper>
  );
};

export default ButtonPage;
