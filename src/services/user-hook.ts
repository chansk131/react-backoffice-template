/** @format */

import { useEffect } from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import useSWR from "swr";
import alertState from "../components/Alert/alertState";
import { userState } from "./auth-hook";
import { IUser } from "./user.types";

interface IUserHook {
  userInfo: IUser | null;
}

function useUser(): IUserHook {
  const user = useRecoilValue(userState);
  const { data, error } = useSWR(user ? "/user-info" : null);
  const setAlertState = useSetRecoilState(alertState);
  const userInfo = {
    username: data?.name,
    age: data?.age,
    company: data?.company,
    role: data?.role,
  };

  useEffect(() => {
    if (error) {
      setAlertState({ message: error.message, type: "error" });
    }
  }, [error, setAlertState]);
  return { userInfo };
}

export default useUser;
