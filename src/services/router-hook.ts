/** @format */
import {
  useParams,
  useLocation,
  useHistory,
  useRouteMatch,
} from "react-router-dom";

interface IUseRouter {
  push: any;
  replace: any;
  pathname: any;
  query: any;
  location: any;
  history: any;
  match: any;
}

function useRouter(): IUseRouter {
  const params = useParams();
  const location = useLocation();
  const history = useHistory();
  const match = useRouteMatch();

  return {
    push: history.push,
    replace: history.replace,
    pathname: location.pathname,
    query: {
      ...params,
    }, // ...queryString.parse(location.search), // Convert string to object
    match,
    location,
    history,
  };
}

export default useRouter;
