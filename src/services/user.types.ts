/** @format */

export interface IUser {
  username: string;
  name?: string;
  company?: string;
  role?: string;
  age?: string;
  password?: string;
}
