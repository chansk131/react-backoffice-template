/** @format */

import axios, { setAxiosHeaderAuthorization } from "../axios";

describe("bearer token", () => {
  test("set bearer token", () => {
    const bearerToken = "bearerToken";
    setAxiosHeaderAuthorization(bearerToken);
    expect(axios.defaults.headers.common.Authorization).toBe(bearerToken);
  });

  test("remove bearer token", () => {
    const bearerToken = "";
    setAxiosHeaderAuthorization(bearerToken);
    expect(axios.defaults.headers.common.Authorization).toBeFalsy();
  });
});
