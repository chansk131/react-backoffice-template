/** @format */
import React from "react";
import { renderHook } from "@testing-library/react-hooks";
// eslint-disable-next-line camelcase
import { RecoilRoot } from "recoil";
import { setupServer } from "msw/node";
import { rest } from "msw";
import { cache, SWRConfig } from "swr";
import useUser from "../user-hook";
import "@testing-library/jest-dom/extend-expect";
import { userState } from "../auth-hook";

const USER_INFO = {
  name: "Theerapat",
  age: 25,
  company: "KBTG",
  role: "Innovation Engineer",
};

const server = setupServer(
  rest.get("/user-info", (req, res, ctx) => {
    return res(ctx.json(USER_INFO));
  })
);

const wrapper: React.FC = ({ children }) => (
  <RecoilRoot
    initializeState={(snap) => snap.set(userState, { username: "Theerapat" })}
  >
    <SWRConfig value={{ dedupingInterval: 0 }}>{children}</SWRConfig>
  </RecoilRoot>
);

beforeAll(() => server.listen());
beforeEach(() => server.resetHandlers());
afterEach(() => cache.clear());
afterAll(() => server.close());

test("empty user get undefiend userInfo", async () => {
  const { result } = renderHook(() => useUser(), {
    wrapper: RecoilRoot,
  });

  const userInfo = {
    username: undefined,
    age: undefined,
    company: undefined,
    role: undefined,
  };

  expect(result.current.userInfo).toEqual(userInfo);
});
test("should get userInfo", async () => {
  const { result, waitForNextUpdate } = renderHook(() => useUser(), {
    wrapper,
  });

  const userInfo = {
    username: "Theerapat",
    age: 25,
    company: "KBTG",
    role: "Innovation Engineer",
  };

  await waitForNextUpdate();
  expect(result.current.userInfo).toEqual(userInfo);
});

test("should get error", async () => {
  server.use(
    rest.get("/user-info", (req, res, ctx) => {
      return res.networkError("Failed to connect");
    })
  );
  const { result, waitForNextUpdate } = renderHook(() => useUser(), {
    wrapper,
  });

  const userInfo = {
    username: undefined,
    age: undefined,
    company: undefined,
    role: undefined,
  };

  await waitForNextUpdate();
  expect(result.current.userInfo).toEqual(userInfo);
});
