/** @format */

import { useCallback, useState } from "react";
import {
  useRecoilState,
  atom,
  SetterOrUpdater,
  useSetRecoilState,
} from "recoil";
import alertState from "../components/Alert/alertState";
import axios, { setAxiosHeaderAuthorization } from "./axios";
import { IUser } from "./user.types";

export const userState = atom<IUser | null>({
  key: "user",
  default: null,
});

interface IUseAuth {
  isChecking: boolean;
  user: IUser | null;
  setUser: SetterOrUpdater<IUser | null>;
  login: ({ username, password }: IUser) => Promise<void>;
  logout: () => Promise<void>;
  refresh: (withoutRefresh?: boolean) => Promise<void>;
}

function useAuth(): IUseAuth {
  const [user, setUser] = useRecoilState(userState);
  const setAlertState = useSetRecoilState(alertState);
  const [isChecking, setIsChecking] = useState(true);

  const getUser = useCallback(async (): Promise<void> => {
    try {
      const respUser = await axios.get("/user-info");
      setUser({ username: respUser.data.name });
    } catch (e) {
      if (e?.response?.status !== 401) {
        setAlertState({ message: `Refresh: ${e.message}`, type: "error" });
      }
    }
  }, [setAlertState, setUser]);

  const refresh = useCallback(
    async (withoutRefresh?: boolean): Promise<void> => {
      if (!withoutRefresh) {
        setIsChecking(true);
      }
      try {
        const resp = await axios.get("refresh");
        setAxiosHeaderAuthorization(resp.data.accessToken);
        if (resp.data.accessToken) {
          await getUser();
        }
      } catch (e) {
        if (e?.response?.status !== 401) {
          setAlertState({ message: `Refresh: ${e.message}`, type: "error" });
        }
      } finally {
        console.log("finish refreshing");
        if (!withoutRefresh) {
          setIsChecking(false);
        }
      }
    },
    [getUser, setAlertState]
  );

  const login = useCallback(
    async ({ username, password }: IUser): Promise<void> => {
      try {
        await axios.post("/login", { username, password });
        await refresh(true);
      } catch (e) {
        setAlertState({ message: `Login: ${e.message}`, type: "error" });
      }
    },
    [refresh, setAlertState]
  );

  const logout = useCallback(async (): Promise<void> => {
    try {
      await axios.get("/logout");
      setAxiosHeaderAuthorization();
      setUser(null);
    } catch (e) {
      setAlertState({ message: `Logout: ${e.message}`, type: "error" });
    }
  }, [setAlertState, setUser]);

  return {
    user,
    isChecking,
    setUser,
    login,
    logout,
    refresh,
  };
}

export default useAuth;
