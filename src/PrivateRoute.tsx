import React from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";
import PageWrapper from "./components/layout/PageWrapper";
import useAuth from "./services/auth-hook";

const PrivateRoute: React.FC<RouteProps> = ({ exact, path, children }) => {
  const { user, logout } = useAuth();
  return (
    <Route
      exact={exact}
      path={path}
      render={({ location }) =>
        user ? (
          <PageWrapper
            hasSider
            user={user}
            username="Kasikorn Rakthai"
            email="email@kbtg.tech"
            avatar=""
            logout={logout}
          >
            {children}
          </PageWrapper>
        ) : (
          <Redirect to={{ pathname: "/login", state: { from: location } }} />
        )
      }
    />
  );
};

export default PrivateRoute;
