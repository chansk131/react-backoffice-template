import React from "react";
import { render, screen } from "@testing-library/react";
import { RecoilRoot } from "recoil";
import App from "./App";

test("renders learn react link", () => {
  render(
    <RecoilRoot>
      <App />
    </RecoilRoot>
  );
  const loadingElement = screen.getByText(/loading/i);
  expect(loadingElement).toBeInTheDocument();
});
