import React, { FC, InputHTMLAttributes, ReactChild } from "react";
import styled from "styled-components";
import Color from "../../theme/color";
import Typography from "../Typo";

export interface Props extends InputHTMLAttributes<HTMLInputElement> {
  children?: ReactChild;
  variant?: "contained" | "outlined";
  color?: "default" | "primary" | "error";
  dense?: boolean;
  label?: string;
  description?: string;
}

interface ILabel {
  disabled?: boolean;
}

const BACKGROUND = "#838e9a";
const DISABLED_BACKGROUND = "#B2B8C0";

const StyledLabel = styled.label<ILabel>`
  display: flex;
  flex-direction: column;
  position: relative;
  padding-left: 24px;
  margin-bottom: 12px;
  font-size: 16px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  color: ${({ disabled }) => (disabled ? BACKGROUND : Color.text.main)};
  /* On mouse-over, add a grey background color */
  &:hover input ~ span {
    border-radius: 1px;
    border: 1px solid
      ${({ disabled }) => (disabled ? DISABLED_BACKGROUND : Color.default.dark)};
  }
  /* Style the indicator (dot/circle) */
  &:hover input:checked ~ span {
    background-color: ${({ disabled }) =>
      disabled ? DISABLED_BACKGROUND : Color.default.dark};
  }

  p {
    margin: 0;
    padding: 0;
    margin-top: 4px;
    font-size: 14px;
    color: ${Color.text.light};
  }
`;

const StyledInput = styled.input<Props>`
  /* Hide the browser's default radio button */
  position: absolute;
  opacity: 0;
  cursor: pointer;
  /* Show the indicator (dot/circle) when checked */
  &:checked ~ span:after {
    display: block;
  }

  &:checked ~ span {
    border: 1px solid
      ${({ disabled }) => (disabled ? DISABLED_BACKGROUND : Color.default.main)};
    background-color: ${({ disabled }) =>
      disabled ? DISABLED_BACKGROUND : Color.default.main};
  }
`;

const StyledSpan = styled.span<ILabel>`
  /* Create a custom radio button */
  position: absolute;
  top: 3px;
  left: 0;
  height: 14px;
  width: 14px;
  border-radius: 1px;
  border: 1px solid
    ${({ disabled }) => (disabled ? DISABLED_BACKGROUND : BACKGROUND)};
  background-color: ${({ disabled }) => (disabled ? "#EFF1F2" : "none")};

  /* Create the indicator (the dot/circle - hidden when not checked) */
  &:after {
    content: "";
    position: absolute;
    display: none;
    /* Style the indicator (dot/circle) */
    left: 4px;
    top: 1px;
    width: 4px;
    height: 8px;
    border: solid white;
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
`;

// Please do not use types off of a default export module or else Storybook Docs will suffer.
// see: https://github.com/storybookjs/storybook/issues/9556
/**
 * A custom Thing component. Neat!
 */
const Checkbox: FC<Props> = ({
  children,
  variant = "contained",
  color = "default",
  dense = false,
  label,
  description,
  disabled,
  ...rest
}) => {
  return (
    <StyledLabel disabled={disabled}>
      {label}
      <StyledInput type="checkbox" disabled={disabled} {...rest} />
      <StyledSpan disabled={disabled} />
      <Typography variant="p">{description}</Typography>
    </StyledLabel>
  );
};

export default Checkbox;
