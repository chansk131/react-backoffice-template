import React, { CSSProperties, FC, ReactChild, useState } from "react";
import styled from "styled-components";
import Color from "../../theme/color";
import { Typo } from "../Typo";
import CloseTagIcon from "../Icon/icons/CloseTagIcon";

interface StyleProps {
  color?: string | undefined;
  backgroundColor?: string;
  borderColor?: string;
  showBorder?: boolean;
  showClose?: boolean;
  icon?: string | ReactChild;
  showIcon?: boolean;
  showPointer?: boolean;
}
export interface TagProps extends Omit<StyleProps, "showIcon | showPointer"> {
  children?: ReactChild;
  style?: CSSProperties;
  text?: string;
  onClick?: () => void;
  onClose?: () => void;
}

const TagContainer = styled.div<TagProps>`
  background: ${({ backgroundColor }) => backgroundColor};
  border: ${({ borderColor, showBorder }) =>
    showBorder ? `1px solid ${borderColor}` : `0px solid #fff`};
  border-radius: 2px;
  width: fit-content;
  padding: 2px 8px;
  height: 21px;
  font-size: 12px;
  ${({ showPointer }) => showPointer && `cursor: pointer`};
`;
const TagText = styled.div<StyleProps>`
  display: grid;
  grid-template-columns: ${({ showClose, showIcon }) =>
    `${showIcon ? "auto " : ""}1fr${showClose ? " 10px" : ""}`};
  grid-gap: 6px;
  align-items: center;
`;
const IconTag = styled.span<StyleProps>`
  ${({ showPointer }) => showPointer && `cursor: pointer`};
  svg,
  img,
  div {
    width: 10px;
    height: 10px;
  }
`;

const Tag: FC<TagProps> = ({
  children,
  text,
  color = "default",
  backgroundColor,
  borderColor,
  icon,
  showBorder = true,
  showClose = false,
  onClick,
  onClose,
  style,
}) => {
  const colorMainCode = color
    ? Color[color]?.main || Color[color] || color
    : "#000000";

  const baseColorBackgroundCode = color
    ? Color[color]?.backgroud || backgroundColor || Color[color] || color
    : "#ffffff";
  const colorBackgroundCode = backgroundColor
    ? Color[backgroundColor] || backgroundColor
    : baseColorBackgroundCode;

  const baseColorBorderCode = color
    ? Color[color]?.border || borderColor || Color[color] || color
    : "#000000";
  const colorBorderCode = borderColor
    ? Color[borderColor] || borderColor
    : baseColorBorderCode;

  const [tagShow, setTagShow] = useState(true);
  const handleOnClose = () => {
    if (typeof onClose === "function") onClose();
    setTagShow(!tagShow);
  };

  if (!tagShow) return null;
  return (
    <TagContainer
      style={style}
      color={colorMainCode}
      backgroundColor={colorBackgroundCode}
      borderColor={colorBorderCode}
      showBorder={showBorder}
      onClick={typeof onClick === "function" ? onClick : () => null}
      showPointer={typeof onClick === "function"}
    >
      {text ? (
        <TagText showClose={showClose} showIcon={Boolean(icon)}>
          {icon && <IconTag>{icon}</IconTag>}
          <Typo variant="span" color={colorMainCode}>
            {text}
          </Typo>
          {showClose && (
            <IconTag showPointer onClick={handleOnClose}>
              <CloseTagIcon color="#838E9A" />
            </IconTag>
          )}
        </TagText>
      ) : (
        children
      )}
    </TagContainer>
  );
};

export default Tag;
