import React, { CSSProperties } from "react";
import styled from "styled-components";
import AvartarIcon from "../Icon/AvatarIcon";

export interface AvatarProps {
  size?: string;
  invisible?: boolean;
  style?: CSSProperties;
  src?: string;
  borderRadius?: string;
}

const AvatarContainer = styled.div<AvatarProps>`
  width: ${({ size }) => size || "40px"};
  height: ${({ size }) => size || "40px"};
  border-radius: ${({ borderRadius }) => borderRadius || "20px"};

  img {
    width: ${({ size }) => size || "40px"};
    height: ${({ size }) => size || "40px"};
  }
`;

const EmptyAvatar = styled.div``;

const Avatar: React.FC<AvatarProps> = ({
  children,
  invisible = true,
  src,
  ...props
}) => {
  if (!invisible) return <EmptyAvatar />;
  return (
    <AvatarContainer {...props}>
      {src ? <img src={src} alt="avartar" /> : children || <AvartarIcon />}
    </AvatarContainer>
  );
};

export default Avatar;
