import { Button } from "antd";
import React, { ReactChild, useEffect, useRef, useState } from "react";
// import { useTranslation } from "react-i18next";
import styled from "styled-components";
import Const from "./Const";
import SiderNavigateIcon from "../Icon/SiderNavigateIcon";
import KbtgIcon from "../Icon/KbtgIcon";
import Avatar from "../Avatar";
import Badge from "../Badge";
import Color from "../../theme/color";
import { Typo } from "../Typo";

interface SubMenuProps {
  onClick?: () => void;
  icon?: ReactChild;
  text?: ReactChild;
}
export type HeaderProp = {
  onOpenSlider?: () => void;
  hasSider?: boolean;
  headerContent?: ReactChild;
  hideAvatar?: boolean;
  avatar?: ReactChild;
  username?: string;
  email?: string;
  subMenu?: Array<SubMenuProps>;
};

const StyledHeader = styled.header`
  z-index: 1201;
  background-color: white;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  padding: 0 22px;
  border-bottom: solid 1px #f1f1f1;
`;
const HeaderContent = styled.div<HeaderProp>`
  width: 100%;
  min-height: ${Const.HeaderHeight}px;
  align-items: center;
  justify-content: space-between;
  display: grid;
  grid-template-columns: ${({ hasSider }) =>
    hasSider ? "auto auto 1fr auto" : "auto 1fr auto"};
  grid-gap: 20px;
`;

const HeaderLogo = styled.div`
  height: 40px;
  display: grid;
  grid-template-columns: auto auto auto;
  grid-gap: 8px;

  svg {
    height: 40px;
  }
`;
const LogoSeperate = styled.div`
  width: 1px;
  height: 40px;
  background: #e0e3e6;
  border-radius: 8px;
`;

const HeaderChildren = styled.div``;

// const LanguageButton = styled(Button)`
//   padding: 4px 6px;
// `;
const SliderButton = styled(Button)`
  padding: 0px 0px;
  border: 0px;
  height: 22px;
`;

const AvatarContent = styled.div``;
const AvatarImage = styled.div`
  cursor: pointer;
`;
interface AvataMenuContentMenuProps {
  open?: boolean;
}
const AvataMenuContent = styled.div<AvataMenuContentMenuProps>`
  width: 220px;
  background-color: ${Color.white};
  filter: drop-shadow(0px 6px 20px rgba(80, 91, 103, 0.2));
  border-radius: 4px;
  right: 8px;
  top: 56px;

  position: absolute;

  opacity: 0;
  /* transition: opacity 300ms;*/
  transition: opacity 300ms linear 0.1s;

  ${({ open }) => open && `opacity: 1;`}
`;
const AvataMenuContentHeader = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 8px;
  margin: 8px 12px;
  align-self: center;
  text-align: center;
`;
const AvataMenuContentHeaderBorder = styled.div`
  background: #e0e3e6;
  height: 1px;
  width: 100%;
`;
const AvataMenuContentMenu = styled.div`
  /* margin: 0 12px; */
`;
const AvataMenuContentSubMenu = styled.div`
  display: grid;
  grid-template-columns: 16px 1fr;
  grid-gap: 9px;
  padding: 8px 12px;
  cursor: pointer;
  &:hover {
    background-color: #eff1f2;
  }
`;
const AvataMenuContentSubMenuIcon = styled.div`
  width: 16px;
  height: 16px;
  font-size: 16px;
  align-self: center;

  svg,
  img,
  div {
    width: 16px;
    height: 16px;
    font-size: 16px;
  }
`;
const AvataMenuContentSubMenuText = styled.div`
  align-self: center;
`;

const Header: React.FC<HeaderProp> = ({
  onOpenSlider,
  hasSider = true,
  headerContent,
  hideAvatar = false,
  avatar,
  username,
  email,
  subMenu,
}) => {
  // const { i18n } = useTranslation();
  // const handleChangeLanguage = (language: string): void => {
  //   i18n.changeLanguage(language);
  // };

  const [showAvatarMenu, setShowAvatarMenu] = useState(false);
  const onClickAvatar = (): void => {
    setShowAvatarMenu(!showAvatarMenu);
  };

  const wrapperRef = useRef<HTMLInputElement>(null);
  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function handleClickOutside(event: any) {
      if (
        wrapperRef.current &&
        !wrapperRef.current.contains(event.target) &&
        showAvatarMenu
      ) {
        setShowAvatarMenu(false);
      }
    }

    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [wrapperRef, showAvatarMenu]);

  return (
    <StyledHeader>
      <HeaderContent hasSider={hasSider}>
        {hasSider && (
          <SliderButton onClick={onOpenSlider}>
            <SiderNavigateIcon color="#1E2227" />
          </SliderButton>
        )}
        <HeaderLogo>
          <KbtgIcon />
          <LogoSeperate />
          <KbtgIcon />
        </HeaderLogo>
        <HeaderChildren>{headerContent}</HeaderChildren>

        {!hideAvatar && (
          <AvatarContent ref={wrapperRef}>
            <AvatarImage onClick={onClickAvatar}>
              <Badge badgeContent={999}>
                <Avatar>{avatar}</Avatar>
              </Badge>
            </AvatarImage>
            <AvataMenuContent open={showAvatarMenu}>
              <AvataMenuContentHeader>
                <Avatar style={{ margin: "auto" }}>{avatar}</Avatar>
                {username && (
                  <Typo
                    variant="div"
                    color="#1E2227"
                    fontFamily="BaiJamjuree"
                    size="16px"
                    lineHeight="16px"
                    weight="normal"
                  >
                    {username}
                  </Typo>
                )}
                {email && (
                  <Typo
                    variant="div"
                    color="#838E9A"
                    fontFamily="BaiJamjuree"
                    size="12px"
                    lineHeight="12px"
                    weight="normal"
                  >
                    {email}
                  </Typo>
                )}
              </AvataMenuContentHeader>
              <AvataMenuContentHeaderBorder />
              <AvataMenuContentMenu>
                {subMenu &&
                  Array.isArray(subMenu) &&
                  subMenu.map((_subMenu: SubMenuProps, index) => (
                    <AvataMenuContentSubMenu
                      onClick={_subMenu.onClick ? _subMenu.onClick : () => null}
                    >
                      <AvataMenuContentSubMenuIcon>
                        {_subMenu.icon}
                      </AvataMenuContentSubMenuIcon>
                      <AvataMenuContentSubMenuText>
                        {_subMenu.text}
                      </AvataMenuContentSubMenuText>
                    </AvataMenuContentSubMenu>
                  ))}
              </AvataMenuContentMenu>
            </AvataMenuContent>
          </AvatarContent>
        )}
      </HeaderContent>
    </StyledHeader>
  );
};

export default Header;
