/** @format */

import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import "jest-styled-components";
import React from "react";
import { RecoilRoot } from "recoil";
import PageWrapper from "../PageWrapper";

jest.mock("../Header.tsx");
jest.mock("../Sider");

test("PageWrapper exists", () => {
  const { queryByTestId, queryByText } = render(
    <RecoilRoot>
      <PageWrapper />
    </RecoilRoot>
  );
  const layoutElement = queryByTestId("layout");
  const headerElement = queryByText("HEADER");
  const siderElement = queryByText("SIDER");
  expect(layoutElement).toBeInTheDocument();
  expect(headerElement).toBeInTheDocument();
  expect(siderElement).not.toBeInTheDocument();
});

test("PageWrapper exists", () => {
  const { queryByTestId, queryByText } = render(
    <RecoilRoot>
      <PageWrapper hasSider />
    </RecoilRoot>
  );
  const layoutElement = queryByTestId("layout");
  const headerElement = queryByText("HEADER");
  const siderElement = queryByText("SIDER");
  expect(layoutElement).toBeInTheDocument();
  expect(headerElement).toBeInTheDocument();
  expect(siderElement).toBeInTheDocument();
});
