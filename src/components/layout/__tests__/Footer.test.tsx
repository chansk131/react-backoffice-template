/** @format */

import React from "react";
import { render } from "@testing-library/react";
import Footer from "../Footer";
import "jest-styled-components";
import "@testing-library/jest-dom/extend-expect";

test("Footer exists", () => {
  const { queryByText } = render(<Footer />);
  const footerText = queryByText("footer");
  expect(footerText).toBeInTheDocument();
});
