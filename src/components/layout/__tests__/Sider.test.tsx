/** @format */

import "@testing-library/jest-dom/extend-expect";
import { fireEvent, render } from "@testing-library/react";
import "jest-styled-components";
import React from "react";
import { RecoilRoot } from "recoil";
import Sider from "../Sider";

test("Sider exists", () => {
  const { queryByTestId } = render(
    <RecoilRoot>
      <Sider />
    </RecoilRoot>
  );
  const sider = queryByTestId("sider");
  expect(sider).toBeInTheDocument();
});

// test("Logout", () => {
//   const { queryByTestId, getByRole } = render(
//     <RecoilRoot>
//       <Sider />
//     </RecoilRoot>
//   );
//   const sider = queryByTestId("sider");
//   expect(sider).toBeInTheDocument();
//   const logoutButton = getByRole("menuitem", { name: "Logout" });
//   fireEvent.click(logoutButton);
// });
