import React, { useState } from "react";
import styled from "styled-components";
import { Menu } from "antd";
import SubMenu from "antd/lib/menu/SubMenu";
import {
  UserOutlined,
  LaptopOutlined,
  NotificationOutlined,
} from "@ant-design/icons";
import useAuth from "../../services/auth-hook";
import SiderNavigateMenuIcon from "../Icon/SiderNavigateMenuIcon";
import Const from "./Const";
import Color from "../../theme/color";

export interface CustomSelectSliderProps {
  isCollapsed?: boolean;
  color?: string | null;
}

const CustomMenu = styled(Menu)<CustomSelectSliderProps>`
  height: calc(100vh - ${Const.HeaderHeight}px);
  overflow-y: scroll;
  width: ${({ inlineCollapsed }) =>
    inlineCollapsed ? Const.SliderWidthOnClose : Const.SliderWidthOnOpen}px;

  .ant-menu-submenu-selected .ant-menu-submenu-title,
  .ant-menu-item-active {
    color: ${({ color }) => color};
  }
  .slider-navigate-icon {
    float: right;
    height: 100%;
  }
  .ant-menu-submenu
    .ant-menu-submenu-title[aria-expanded="true"]
    .slider-navigate-icon {
    transition: transform 0.2s ease-in-out;
    transform: rotate(90deg);
  }
  .ant-menu-submenu
    .ant-menu-submenu-title[aria-expanded="false"]
    .slider-navigate-icon {
    transition: transform 0.2s ease-in-out;
    transform: rotate(0deg);
  }
  .ant-menu-submenu-active .ant-menu-submenu-title {
    color: ${({ color }) => color};
  }

  .ant-menu-submenu-selected svg path {
    fill: ${({ color }) => color};
  }
`;
const CustomSubMenu = styled(SubMenu)<CustomSelectSliderProps>`
  color: ${Color.white};
  ul {
    background: #282e34 !important;
  }
  .ant-menu-item-selected {
    color: ${Color.white} !important;
    background: ${({ color }) => color} !important;
  }
  .ant-menu-item-selected::after {
    border: 0px;
  }
  .ant-menu-item-active {
    color: ${({ color }) => color};
  }
`;
const CustomMenuItem = styled(Menu.Item)`
  color: ${Color.white};
`;

const CustomSider: React.FC<CustomSelectSliderProps> = ({
  isCollapsed = false,
  color = "primary",
  ...prop
}) => {
  const colorHexCode = color
    ? Color[color]?.main || Color[color] || color
    : "#ffffff";
  const { logout } = useAuth();
  const handleLogout = () => {
    logout();
  };
  const [siderSetting, setSiderSetting] = useState({
    openKey: "sub1",
    selectedKey: "1",
  });

  return (
    <CustomMenu
      mode="inline"
      defaultSelectedKeys={[siderSetting.selectedKey]}
      selectedKeys={[siderSetting.selectedKey]}
      defaultOpenKeys={[siderSetting.openKey]}
      // openKeys={["sub1"]}
      style={{
        height: "100%",
        borderRight: 0,
        background: "#1E2227",
      }}
      inlineCollapsed={isCollapsed}
      expandIcon={<SiderNavigateMenuIcon />}
      inlineIndent={20}
      color={colorHexCode}
    >
      <CustomSubMenu
        key="sub1"
        icon={
          <UserOutlined
            style={{
              color:
                siderSetting.openKey === "sub1" ? colorHexCode : Color.white,
            }}
          />
        }
        title="subnav 1"
        color={colorHexCode}
      >
        <CustomMenuItem key="1">option1</CustomMenuItem>
        <CustomMenuItem key="2">option2</CustomMenuItem>
        <CustomMenuItem key="3">option3</CustomMenuItem>
        <CustomMenuItem key="4">option4</CustomMenuItem>
      </CustomSubMenu>
      <CustomSubMenu
        key="sub2"
        icon={<LaptopOutlined style={{ color: "#fff" }} />}
        title="subnav 2"
      >
        <CustomMenuItem key="5">option5</CustomMenuItem>
        <CustomMenuItem key="6">option6</CustomMenuItem>
        <CustomMenuItem key="7">option7</CustomMenuItem>
        <CustomMenuItem key="8">option8</CustomMenuItem>
      </CustomSubMenu>
      <CustomSubMenu
        key="sub3"
        icon={<NotificationOutlined style={{ color: "#fff" }} />}
        title="subnav 3"
      >
        <CustomMenuItem key="9">option9</CustomMenuItem>
        <CustomMenuItem key="10">option10</CustomMenuItem>
        <CustomMenuItem key="11">option11</CustomMenuItem>
        <CustomMenuItem key="12">option12</CustomMenuItem>
      </CustomSubMenu>
      <CustomMenuItem key="logout" onClick={handleLogout}>
        Logout
      </CustomMenuItem>
    </CustomMenu>
  );
};

export default CustomSider;
