import React, { ReactChild, useState } from "react";
// import { useLocation } from "react-router-dom";
import styled from "styled-components";
import Layout, { Content } from "antd/lib/layout/layout";
import userRouter from "../../services/router-hook";
import Alert from "../Alert";
import Header from "./Header";
import Sider from "./Sider";
import Const from "./Const";
import BulbIcon from "../Icon/icons/BulbIcon";
import LogoutIcon from "../Icon/icons/LogoutIcon";
import { Typo } from "../Typo";
import { IUser } from "../../services/user.types";
// import Footer from './Footer'

const StyledLayout = styled(Layout)`
  height: 100vh;
  width: 100vw;
`;
const LayoutContent = styled.div`
  display: grid;
  grid-template-columns: ${({ hasSider }: PageWrapperProp) =>
    hasSider ? "auto 1fr" : "1fr"};
  grid-gap: 24px;
  padding-top: 64px;
`;

type PageWrapperProp = {
  user?: IUser;
  hasSider?: boolean;
  username?: string;
  email?: string;
  avatar?: ReactChild;
  logout?: () => void;
};

const PageWrapper: React.FC<PageWrapperProp> = ({
  children,
  user,
  hasSider,
  username,
  email,
  avatar,
  logout,
  ...props
}) => {
  const { pathname } = userRouter();
  const [sliderOpen, setSliderOpen] = useState(false);
  const onOpenSlider = () => {
    setSliderOpen(!sliderOpen);
  };

  const isLogin = Boolean(user);

  return (
    <StyledLayout data-testid="layout">
      <Header
        onOpenSlider={onOpenSlider}
        hasSider={hasSider || isLogin}
        hideAvatar={pathname === "/login" || !isLogin}
        avatar={avatar}
        username={username}
        email={email}
        subMenu={[
          {
            icon: <BulbIcon color="#838E9A" />,
            text: (
              <Typo
                variant="div"
                color="text"
                fontFamily="BaiJamjuree"
                size="16px"
                lineHeight="24px"
                weight="normal"
              >
                ช่วยเหลือ
              </Typo>
            ),
          },
          {
            onClick: logout,
            icon: <LogoutIcon color="#DB302F" />,
            text: (
              <Typo
                variant="div"
                color="#DB302F"
                fontFamily="BaiJamjuree"
                size="16px"
                lineHeight="24px"
                weight="normal"
              >
                ออกจากระบบ
              </Typo>
            ),
          },
        ]}
      />

      <LayoutContent hasSider={hasSider}>
        {hasSider && <Sider isCollapsed={sliderOpen} />}
        <Layout
          style={{
            padding: "0 24px 24px",
            height: `calc(100vh - ${Const.HeaderHeight}px)`,
            overflowY: "scroll",
          }}
        >
          <Content
            className="site-layout-background"
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            {children}
            <Alert />
          </Content>
        </Layout>
      </LayoutContent>
    </StyledLayout>
  );
};

export default PageWrapper;
