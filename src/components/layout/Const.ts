/** @format */

const Const = {
  HeaderHeight: 64,
  SliderWidthOnClose: 64,
  SliderWidthOnOpen: 264,
};
export default Const;
