export interface IconProp {
  isActive?: boolean;
  color?: string;
  colorActive?: string;
  classname?: string;
  id?: string;
}
