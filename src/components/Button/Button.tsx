import React, { FC, ButtonHTMLAttributes, ReactChild } from "react";
import styled, { css } from "styled-components";
import Color from "../../theme/color";

export interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
  children?: ReactChild;
  variant?: "contained" | "outlined";
  color?: "default" | "primary" | "error" | undefined;
  dense?: boolean;
}

const StyledButton = styled.button<Props>`
  cursor: pointer;
  box-sizing: border-box;
  border: none;
  border-radius: 4px;
  transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  &:focus {
    box-shadow: 0px 1px 5px rgba(25, 118, 210, 0.3);
    outline: none;
  }

  ${({ disabled, variant, dense, color = "default" }) => {
    if (disabled) {
      return css`
        padding: 10px 24px;
        color: #b2b8c0;
        background-color: #e0e3e6;
        cursor: pointer;
        &:focus {
          box-shadow: none;
        }
      `;
    }

    switch (variant) {
      case "outlined": {
        return css`
          padding: ${dense ? "3px 9px" : "9px 23px"};
          border: 1px solid ${Color[color].main};
          color: ${Color[color].main};
          background-color: white;
          &:hover {
            background-color: ${Color[color].lighter};
          }
          &:active {
            border: 1px solid ${Color[color].dark};
            color: ${Color[color].dark};
            background-color: ${Color[color].lighter};
          }
        `;
      }
      case "contained": {
        return css`
          padding: ${dense ? "4px 10px" : "10px 24px"};
          color: white;
          background-color: ${Color[color].main};
          &:hover {
            background-color: ${Color[color].light};
          }
          &:active {
            background-color: ${Color[color].dark};
          }
        `;
      }
      default:
        return "";
    }
  }}
`;

// Please do not use types off of a default export module or else Storybook Docs will suffer.
// see: https://github.com/storybookjs/storybook/issues/9556
/**
 * A custom Thing component. Neat!
 */
export const Button: FC<Props> = ({
  children,
  variant = "contained",
  color = "default",
  dense = false,
  disabled,
  type = "button",
  ...rest
}) => {
  return (
    <StyledButton
      disabled={disabled}
      variant={variant}
      dense={dense}
      color={color}
      type={type}
      {...rest}
    >
      {children}
    </StyledButton>
  );
};
