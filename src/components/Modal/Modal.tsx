import React, {
  CSSProperties,
  FC,
  ReactChild,
  useEffect,
  useRef,
  useState,
} from "react";
import styled from "styled-components";
import Color from "../../theme/color";
import { Typo } from "../Typo";
import CloseTagIcon from "../Icon/icons/CloseTagIcon";
import { Button } from "../Button/Button";
import WarningModalIcon from "../Icon/icons/WarningModalIcon";
import SuccessModalIcon from "../Icon/icons/SuccessModalIcon";
import ErrorModalIcon from "../Icon/icons/ErrorModalIcon";
import InfoModalIcon from "../Icon/icons/InfoModalIcon";

interface StyleProps {
  transition?: "up" | "down" | "left" | "right";
  open?: boolean;
  type?: "info" | "success" | "warning" | "error" | undefined;
}
export interface ModalProps extends Omit<StyleProps, "showIcon | showPointer"> {
  style?: CSSProperties;
  title?: string | ReactChild;
  children?: ReactChild;
  text?: string;
  showSubmit?: boolean;
  submitStyle?: CSSProperties;
  submitText?: string | ReactChild;
  submitColor?: "default" | "primary" | "error" | undefined;
  onSubmit?: () => void;
  showCancel?: boolean;
  cancelStyle?: CSSProperties;
  cancelText?: string | ReactChild;
  cancelColor?: "default" | "primary" | "error" | undefined;
  onCancel?: () => void;
  showClose?: boolean;
  onClose?: () => void;
}

type ModalTitleProps = Pick<
  ModalProps,
  "title" | "showClose" | "onClose" | "type"
>;
const ModalTitleContainer = styled.div<ModalTitleProps>`
  display: grid;
  grid-template-columns: ${({ type, showClose }) =>
    `${type ? "auto " : ""}1fr${showClose ? " auto" : ""}`};
  min-height: 36px;

  ${({ type }) =>
    type
      ? `
        font-size: 16px;
        line-height: 19px;`
      : `font-size: 22px;
        line-height: 26px;`}

  padding: ${({ type }) => (type ? "24px 24px 0px 24px" : "24px")};
`;
const CloseIconContainer = styled.span`
  display: block;
  height: auto;
  cursor: pointer;
  align-self: start;
  font-size: 10px;
`;
const IconContainer = styled.span`
  display: block;
  height: auto;
  align-self: start;
  font-size: 24px;
  padding-right: 8px;
`;

const TagOnly = styled.span`
  display: block;
  height: auto;
`;
const ModalTitle: FC<ModalTitleProps> = ({
  type,
  title,
  showClose,
  onClose,
}) => {
  return (
    <ModalTitleContainer type={type} showClose={showClose}>
      {type && (
        <IconContainer>
          {type === "warning" && <WarningModalIcon />}
          {type === "success" && <SuccessModalIcon />}
          {type === "error" && <ErrorModalIcon />}
          {type === "info" && <InfoModalIcon />}
        </IconContainer>
      )}

      {typeof title === "string" ? (
        <Typo variant="div" color="text" wordBreak="break-word">
          {title}
        </Typo>
      ) : (
        title || <TagOnly />
      )}
      {showClose && (
        <CloseIconContainer onClick={onClose}>
          <CloseTagIcon color="#838E9A" />
        </CloseIconContainer>
      )}
    </ModalTitleContainer>
  );
};

const ModalBackgroundContainer = styled.div<StyleProps>`
  ${({ open }) =>
    open &&
    `width: 100vw;
    height: 100vh;

    background: #333;
    position: absolute;
    top: 0px;
    left: 0px;
    z-index: 9999;
    opacity: 0.5;
    backdrop-filter: blur(12px) !important;
    -webkit-backdrop-filter: blur(12px);`}

  ${({ open, transition }) =>
    open
      ? `opacity:0;
    animation: ${`scale-${transition}-modal-bg-open`} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;`
      : `animation: ${`scale-${transition}-modal-bg-down`} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;`}

  @keyframes scale-up-modal-bg-open {
    0% {
      transform: scale(1) translate(0%, 150%);
      opacity: 0;
    }
    100% {
      transform: scale(1) translate(0%, 0%);
      opacity: 0.5;
    }
  }
  @keyframes scale-up-modal-bg-close {
    0% {
      transform: scale(1) translate(0%, 0%);
      opacity: 0.5;
    }
    100% {
      transform: scale(1) translate(0%, 150%);
      opacity: 0;
    }
  }
  @keyframes scale-down-modal-bg-open {
    0% {
      transform: scale(1) translate(0%, -150%);
      opacity: 0;
    }
    100% {
      transform: scale(1) translate(0%, 0%);
      opacity: 0.5;
    }
  }
  @keyframes scale-down-modal-bg-close {
    0% {
      transform: scale(1) translate(0%, 0%);
      opacity: 0.5;
    }
    100% {
      transform: scale(1) translate(0%, -150%);
      opacity: 0;
    }
  }
  @keyframes scale-left-modal-bg-open {
    0% {
      transform: scale(1) translate(-150%, 0%);
      opacity: 0;
    }
    100% {
      transform: scale(1) translate(0%, 0%);
      opacity: 0.5;
    }
  }
  @keyframes scale-left-modal-bg-close {
    0% {
      transform: scale(1) translate(0%, 0%);
      opacity: 0.5;
    }
    100% {
      transform: scale(1) translate(-150%, 0%);
      opacity: 0;
    }
  }
  @keyframes scale-right-modal-bg-open {
    0% {
      transform: scale(1) translate(150%, 0%);
      opacity: 0;
    }
    100% {
      transform: scale(1) translate(0%, 0%);
      opacity: 0.5;
    }
  }
  @keyframes scale-right-modal-bg-close {
    0% {
      transform: scale(1) translate(0%, 0%);
      opacity: 0.5;
    }
    100% {
      transform: scale(1) translate(150%, 0%);
      opacity: 0;
    }
  }
`;
const ModalContainer = styled.div<StyleProps>`
  width: 572px;
  min-height: 36px;
  background: ${Color.white};
  box-shadow: 0px 6px 20px rgba(80, 91, 103, 0.2);
  border-radius: 4px;

  position: fixed;
  top: 50%;
  left: 50%;

  display: grid;
  grid-template-columns: 1fr;
  z-index: 99999;

  ${({ open, transition }) =>
    open
      ? `opacity:0;
    animation: ${`scale-${transition}-modal-open`} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;`
      : `animation: ${`scale-${transition}-modal-close`} .5s cubic-bezier(0.165, 0.840, 0.440, 1.000) forwards;`}

  @keyframes scale-up-modal-open {
    0% {
      transform: scale(1) translate(-50%, 200%);
      opacity: 0;
    }
    100% {
      transform: scale(1) translate(-50%, -50%);
      opacity: 1;
    }
  }
  @keyframes scale-up-modal-close {
    0% {
      transform: scale(1) translate(-50%, -50%);
      opacity: 1;
    }
    100% {
      transform: scale(1) translate(-50%, 200%);
      opacity: 0;
    }
  }
  @keyframes scale-down-modal-open {
    0% {
      transform: scale(1) translate(-50%, -250%);
      opacity: 0;
    }
    100% {
      transform: scale(1) translate(-50%, -50%);
      opacity: 1;
    }
  }
  @keyframes scale-down-modal-close {
    0% {
      transform: scale(1) translate(-50%, -50%);
      opacity: 1;
    }
    100% {
      transform: scale(1) translate(-50%, -250%);
      opacity: 0;
    }
  }
  @keyframes scale-left-modal-open {
    0% {
      transform: scale(1) translate(-250%, -50%);
      opacity: 0;
    }
    100% {
      transform: scale(1) translate(-50%, -50%);
      opacity: 1;
    }
  }
  @keyframes scale-left-modal-close {
    0% {
      transform: scale(1) translate(-50%, -50%);
      opacity: 1;
    }
    100% {
      transform: scale(1) translate(-250%, -50%);
      opacity: 0;
    }
  }
  @keyframes scale-right-modal-open {
    0% {
      transform: scale(1) translate(250%, -50%);
      opacity: 0;
    }
    100% {
      transform: scale(1) translate(-50%, -50%);
      opacity: 1;
    }
  }
  @keyframes scale-right-modal-close {
    0% {
      transform: scale(1) translate(-50%, -50%);
      opacity: 1;
    }
    100% {
      transform: scale(1) translate(250%, -50%);
      opacity: 0;
    }
  }
`;
const ModalDesc = styled.div<StyleProps>`
  padding: ${({ type }) => (type ? "0 24px 16px 54px" : "24px")};
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
`;
const ModalBorder = styled.div<StyleProps>`
  width: 100%;
  height: 1px;
  background: #e5e5e5;
`;
const ButtonContainer = styled.div<StyleProps>`
  margin: 24px;
  text-align: right;
`;
const ButtonWrap = styled(Button)`
  margin: 0px 0px 0px 8px;
`;
const Modal: FC<ModalProps> = ({
  transition = "up",
  open = false,
  style,
  type,
  title,
  children,
  text,
  showSubmit = true,
  submitStyle,
  submitText = "Accept",
  submitColor = "primary",
  onSubmit,
  showCancel = true,
  cancelStyle,
  cancelColor = "default",
  cancelText = "Cancel",
  onCancel,
  showClose = true,
  onClose,
}) => {
  const [isOpen, setOpen] = useState(false);
  const openModal = useRef(0);
  const handleOnClose = () => {
    if (typeof onClose === "function") onClose();
    setOpen(false);
  };
  const handleOnCancel = () => {
    if (typeof onCancel === "function") onCancel();
    setOpen(false);
  };
  const handleOnSubmit = () => {
    if (typeof onSubmit === "function") onSubmit();
    setOpen(false);
  };

  useEffect(() => {
    setOpen(open);
  }, [open]);

  if (isOpen) openModal.current += 1;
  if (openModal.current === 0) return <></>;

  return (
    <>
      <ModalBackgroundContainer
        open={isOpen}
        transition={transition}
        onClick={handleOnClose}
      />

      <ModalContainer style={style} transition={transition} open={isOpen}>
        <ModalTitle
          title={title}
          type={type}
          showClose={showClose}
          onClose={handleOnClose}
        />
        <ModalDesc type={type}>
          {text ? (
            <Typo
              variant="div"
              color="default"
              wordBreak="break-word"
              fontFamily="BaiJamjuree"
            >
              {text}
            </Typo>
          ) : (
            children
          )}
        </ModalDesc>
        {(showSubmit || showCancel) && (
          <>
            {!type ? <ModalBorder /> : <></>}
            <ButtonContainer>
              {showCancel && (
                <ButtonWrap
                  color={cancelColor}
                  variant="outlined"
                  type="button"
                  style={cancelStyle}
                  onClick={handleOnCancel}
                >
                  {cancelText}
                </ButtonWrap>
              )}
              {showSubmit && (
                <ButtonWrap
                  color={submitColor}
                  style={submitStyle}
                  onClick={handleOnSubmit}
                >
                  {submitText}
                </ButtonWrap>
              )}
            </ButtonContainer>
          </>
        )}
      </ModalContainer>
    </>
  );
};

export default Modal;
