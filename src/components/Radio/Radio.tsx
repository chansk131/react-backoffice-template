import React, { FC, InputHTMLAttributes, ReactChild } from "react";
import styled, { css } from "styled-components";
import Color from "../../theme/color";
import Typography from "../Typo";

export interface Props extends InputHTMLAttributes<HTMLInputElement> {
  children?: ReactChild;
  variant?: "contained" | "outlined";
  color?: "default" | "primary" | "error";
  dense?: boolean;
  label?: string;
  description?: string;
}

interface ILabel {
  disabled?: boolean;
}

const BACKGROUND = "#838e9a";
const DISABLED_BACKGROUND = "#B2B8C0";

const innerDotStyle = css`
  top: 2px;
  left: 2px;
  width: 8px;
  height: 8px;
  border-radius: 50%;
`;

const StyledLabel = styled.label<ILabel>`
  display: flex;
  flex-direction: column;
  position: relative;
  padding-left: 23px;
  margin-bottom: 12px;
  font-size: 16px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  color: ${({ disabled }) => (disabled ? BACKGROUND : Color.text.main)};
  /* On mouse-over, add a grey background color */
  &:hover input ~ span {
    border: 1px solid
      ${({ disabled }) => (disabled ? DISABLED_BACKGROUND : Color.default.dark)};
  }
  /* Style the indicator (dot/circle) */
  &:hover span:after {
    ${innerDotStyle}
    background: ${({ disabled }) =>
      disabled ? DISABLED_BACKGROUND : Color.default.dark};
  }

  p {
    margin: 0;
    padding: 0;
    margin-top: 4px;
    font-size: 14px;
    color: ${Color.text.light};
  }
`;

const StyledInput = styled.input<Props>`
  /* Hide the browser's default radio button */
  position: absolute;
  opacity: 0;
  cursor: pointer;
  /* Show the indicator (dot/circle) when checked */
  &:checked ~ span:after {
    display: block;
  }
`;

const StyledSpan = styled.span<ILabel>`
  /* Create a custom radio button */
  position: absolute;
  top: 4px;
  left: 0;
  height: 14px;
  width: 14px;
  border-radius: 50%;
  border: 1px solid ${({ disabled }) => (disabled ? "#B2B8C0" : BACKGROUND)};
  background-color: ${({ disabled }) => (disabled ? "#EFF1F2" : null)};

  /* Create the indicator (the dot/circle - hidden when not checked) */
  &:after {
    content: "";
    position: absolute;
    display: none;
    /* Style the indicator (dot/circle) */
    ${innerDotStyle}
    background: ${({ disabled }) => (disabled ? "#B2B8C0" : BACKGROUND)};
  }
`;

const Radio: FC<Props> = ({
  children,
  variant = "contained",
  color = "default",
  dense = false,
  label,
  description,
  disabled,
  value,
  ...rest
}) => {
  return (
    <StyledLabel disabled={disabled}>
      {label}
      <StyledInput type="radio" disabled={disabled} {...rest} />
      <StyledSpan disabled={disabled} />
      <Typography variant="p">{description}</Typography>
    </StyledLabel>
  );
};

export default Radio;
