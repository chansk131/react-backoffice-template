import React, { ReactChild } from "react";
import styled, { css } from "styled-components";
import Color from "../../theme/color";
import { Typo } from "../Typo";
import { TypographyProps } from "../Typo/Typo";

export const VARIANT_BADGE_DOT = "dot";
export const VARIANT_BADGE_STANDARD = "standard";
export const OVERLAP_BADGE_CIRCLE = "circle";
export const VERTICAL_BADGE_TOP = "top";
export const VERTICAL_BADGE_BOTTOM = "bottom";
export const HORIZONTAL_BADGE_RIGHT = "right";
export const HORIZONTAL_BADGE_LEFT = "left";

interface AnchorOriginProps {
  vertical: "top" | "bottom";
  horizontal: "right" | "left";
}
export interface BadgeProps {
  children?: ReactChild;
  variant?: "dot" | "standard";
  height?: number;
  overlap?: "circle" | "rectangle";
  color?: string | undefined;
  borderColor?: string | undefined;
  anchorOrigin?: AnchorOriginProps;
  invisible?: boolean;
  showZero?: boolean;
  badgeContent?: string | number;
  max?: number;
  badgeContentProps?: TypographyProps;
}

const Container = styled.div`
  position: relative;
`;
const styles = css<BadgeProps>`
  height: ${({ height }) => `${height}px`};
  min-width: ${({ height }) => `${height}px`};
  width: auto;
  border-radius: ${({ height, overlap }) =>
    overlap === OVERLAP_BADGE_CIRCLE ? `${height}px` : "0px"};
  background: ${({ color }) => color};
  border: 2px solid ${({ borderColor }) => borderColor};

  position: absolute;
  ${({ anchorOrigin }) =>
    anchorOrigin?.vertical === VERTICAL_BADGE_TOP &&
    anchorOrigin?.horizontal === HORIZONTAL_BADGE_RIGHT &&
    `top: 0;
    right: 0;
    transform: scale(1) translate(50%, -50%);
    transform-origin: 100% 0%;`}
  ${({ anchorOrigin }) =>
    anchorOrigin?.vertical === VERTICAL_BADGE_TOP &&
    anchorOrigin?.horizontal === HORIZONTAL_BADGE_LEFT &&
    `top: 0;
    left: 0;
    transform: scale(1) translate(-50%, -50%);
    transform-origin: 0% 0%;`}
  ${({ anchorOrigin }) =>
    anchorOrigin?.vertical === VERTICAL_BADGE_BOTTOM &&
    anchorOrigin?.horizontal === HORIZONTAL_BADGE_RIGHT &&
    `right: 0;
    bottom: 0;
    transform: scale(1) translate(50%, 50%);
    transform-origin: 100% 100%;`}
  ${({ anchorOrigin }) =>
    anchorOrigin?.vertical === VERTICAL_BADGE_BOTTOM &&
    anchorOrigin?.horizontal === HORIZONTAL_BADGE_LEFT &&
    `left: 0;
    bottom: 0;
    transform: scale(1) translate(-50%, 50%);
    transform-origin: 0% 100%;`}
`;
const BadgeContainer = styled.div<BadgeProps>`
  ${styles}
`;

const Badge: React.FC<BadgeProps> = ({
  children,
  variant = VARIANT_BADGE_STANDARD,
  height,
  overlap = OVERLAP_BADGE_CIRCLE,
  color = "error",
  borderColor = "white",
  anchorOrigin = { vertical: "top", horizontal: "right" },
  invisible = true,
  showZero = false,
  badgeContent,
  max = 99,
  badgeContentProps,
}) => {
  const colorHexCode = color
    ? Color[color]?.main || Color[color] || color
    : "#000000";
  const borderColorHexCode = borderColor
    ? Color[borderColor]?.main || Color[borderColor] || borderColor
    : "#ffffff";
  const heightBadge = height || variant === VARIANT_BADGE_DOT ? 12 : 24;
  const showContentBadge =
    variant === VARIANT_BADGE_DOT ||
    (variant === VARIANT_BADGE_STANDARD &&
      ((showZero && typeof badgeContent === "number" && badgeContent === 0) ||
        (!showZero && typeof badgeContent === "number" && badgeContent > 0) ||
        typeof badgeContent === "string"));

  if (!invisible || !showContentBadge) return <></>;

  const contentBadge =
    typeof badgeContent === "number" && badgeContent > max
      ? `${max}+`
      : badgeContent;

  return (
    <Container>
      <BadgeContainer
        height={heightBadge}
        overlap={overlap}
        color={colorHexCode}
        borderColor={borderColorHexCode}
        anchorOrigin={anchorOrigin}
      >
        {variant === VARIANT_BADGE_DOT ? (
          <></>
        ) : (
          <Typo
            variant="span"
            color="white"
            align="center"
            size="12px"
            lineHeight="22px"
            weight="600"
            margin="0 auto"
            padding="0 6px"
            {...badgeContentProps}
          >
            {contentBadge}
          </Typo>
        )}
      </BadgeContainer>
      {children}
    </Container>
  );
};

export default Badge;
