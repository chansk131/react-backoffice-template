import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import "jest-styled-components";
import React from "react";
import { RecoilRoot } from "recoil";
import Alert from "../Alert";
import alertState from "../alertState";

describe("<Alert />", () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  test("no message", () => {
    const { queryByRole } = render(
      <RecoilRoot>
        <Alert />
      </RecoilRoot>
    );

    const content = queryByRole("alert");
    expect(content).not.toBeInTheDocument();
  });

  test("alert message", async () => {
    const { findByRole } = render(
      <RecoilRoot
        initializeState={(snap) =>
          snap.set(alertState, { message: "alert message" })
        }
      >
        <Alert />
      </RecoilRoot>
    );

    const content = await findByRole("alert");
    expect(content).toBeInTheDocument();
    expect(content).toHaveTextContent("alert message");
  });

  test("alert message disappear in 2000", async () => {
    const { findByRole } = render(
      <RecoilRoot
        initializeState={(snap) =>
          snap.set(alertState, { message: "alert message" })
        }
      >
        <Alert />
      </RecoilRoot>
    );

    const content = await findByRole("alert");
    expect(content).toBeInTheDocument();
    expect(content).toHaveTextContent("alert message");

    expect(setTimeout).toHaveBeenCalledTimes(1);
    expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 2000);
  });
});
