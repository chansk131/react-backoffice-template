import React from "react";
import styled from "styled-components";
import { Tooltip } from "antd";
import { QuestionCircleOutlined } from "@ant-design/icons";
import { LabelTextProps } from "./InputInterface.d";
import { Typo } from "../Typo";

const LabelContainer = styled.div<LabelTextProps>`
  padding: ${({ padding }) => padding || "4px 0"};
  display: grid;
  grid-template-columns: 1fr auto;
  grid-gap: 8px;
  text-align: ${({ labelAlign, align }) => labelAlign || align || "left"};
`;
const LabelText = styled(({ size, lineHeight, color, ...rest }) => (
  <Typo variant="span" {...rest} />
))<LabelTextProps>`
  font-size: ${({ size }) => size || "14px"};
  line-height: ${({ lineHeight }) => lineHeight || "22px"};
  color: ${({ color }) => color || "#505B67 !important"};
`;
const TypoContainer = styled.div`
  margin-bottom: 0px !important;
  /* display: flow-root; */
  display: flex;
`;
const QuestionIcon = styled.span<LabelTextProps>`
  padding-left: 5px;
  color: ${({ labelIconColor }) => labelIconColor || "#000000"};
`;

const Label: React.FC<LabelTextProps> = ({
  text,
  labelHint,
  showCount,
  maxLength,
  counting,
  placement,
  labelIconColor,
  labelAlign,
  ...props
}) => {
  if (!text && ((!showCount && !maxLength) || (showCount && !maxLength)))
    return null;
  return (
    <LabelContainer labelAlign={labelAlign} {...props}>
      {text ? (
        <TypoContainer>
          <LabelText {...props} color="#DB302F">
            *
          </LabelText>
          <LabelText {...props}>{text}</LabelText>
          <Tooltip placement={placement || "top"} title={labelHint || ""}>
            <QuestionIcon labelIconColor={labelIconColor} {...props}>
              <QuestionCircleOutlined />
            </QuestionIcon>
          </Tooltip>
        </TypoContainer>
      ) : (
        <div>&nbsp;</div>
      )}
      {showCount &&
        Number.isInteger(maxLength) &&
        Number.isInteger(counting) && (
          <LabelText {...props}>{`${counting}/${maxLength}`}</LabelText>
        )}
    </LabelContainer>
  );
};

export default Label;
