import React from "react";
import styled from "styled-components";
import { InputNumber } from "antd";
import { InputNumberProps } from "antd/lib/input-number";
import { InputContainer, Label, FormItem, HelperText } from "./index";
import {
  LabelProps,
  CustomFormItemsProps,
  HelperProps,
} from "./InputInterface.d";
import { AlignType } from "../../global/Interface.d";

const InputNumberContainer = styled(InputNumber)<CustomInputnumberStyleProps>`
  border-radius: 4px !important;
  width: 100%;
  input {
    text-align: ${({ inputAlign }) => inputAlign || "right"};
    padding-right: 22px;
  }
`;

interface CustomInputnumberStyleProps extends InputNumberProps {
  inputAlign?: AlignType;
}
interface CustomInputnumbersProps
  extends CustomInputnumberStyleProps,
    LabelProps {}
const CustomInput: React.FC<CustomInputnumbersProps> = ({ label, ...rest }) => {
  return (
    <InputContainer>
      <Label {...label} align={label?.labelAlign} />
      <InputNumberContainer
        {...rest}
        formatter={(value) =>
          `${value}`.replace(/\B(?=(\d{3})+(?!\d)(.*))/g, ",")
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        parser={(value: any) => value.replace(/\$\s?|(,*)/g, "")}
      />
    </InputContainer>
  );
};

interface CustomFromInputnumbersProps
  extends CustomInputnumbersProps,
    CustomFormItemsProps {
  helperText?: HelperProps;
}
const CustomFormInputnumber: React.FC<CustomFromInputnumbersProps> = ({
  formItem,
  helperText,
  ...rest
}) => {
  if (formItem)
    return (
      <>
        <FormItem {...formItem}>
          <CustomInput {...rest} />
        </FormItem>
        {helperText && <HelperText {...helperText} />}
      </>
    );
  return <CustomInput {...rest} />;
};

export default CustomFormInputnumber;
