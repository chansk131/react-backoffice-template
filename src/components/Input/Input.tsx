import React, { useState } from "react";
import styled from "styled-components";
import { Input as Inputtext } from "antd";
import { InputProps } from "antd/lib/input";
import { InternalNamePath } from "antd/lib/form/interface";
import { FormItem, HelperText, InputContainer, Label, Select } from "./index";
import { AlignProps } from "../../global/Interface.d";
import {
  CustomSelectProps,
  RowStyleProps,
  hasSelectInRowProps,
  LabelProps,
  CustomFormItemsProps,
  HelperProps,
} from "./InputInterface.d";

/* INPUT */
const InputTextContainer = styled(({ hasSelectInRow, ...rest }) => (
  <Inputtext {...rest} />
))<CustomInputStyleProps>`
  border-radius: 4px !important;
  ${({ hasSelectInRow }) => {
    return `border-top-left-radius: 0px !important; 
			  border-bottom-left-radius: 0px !important;`;
  }};
  input {
    text-align: ${({ align }) => align || "left"};
  }
`;
const InputPasswordContainer = styled(({ hasSelectInRow, ...rest }) => (
  <Inputtext.Password {...rest} />
))<CustomInputStyleProps>`
  border-radius: 4px !important;
  input {
    text-align: ${({ align }) => align || "left"};
  }
`;
interface CustomInputStyleProps
  extends InputProps,
    AlignProps,
    hasSelectInRowProps {}
interface CustomInputProps extends CustomInputStyleProps {
  texttype: "text" | "password" | "number" | "textarea";
  formatter?: boolean;
}
const Input: React.FC<CustomInputProps> = ({ texttype, onChange, ...rest }) => {
  if (texttype === "password")
    return <InputPasswordContainer onChange={onChange} {...rest} />;
  return <InputTextContainer {...rest} onChange={onChange} />;
};

/* MAIN */
const RowContainer = styled.div<RowStyleProps>`
  display: grid;
  grid-template-columns: ${({ rowGridWidth, hasSelectInRow }) =>
    rowGridWidth || (hasSelectInRow ? "1fr 3fr" : "100%")};
`;
interface CustomInputsProps
  extends CustomInputProps,
    RowStyleProps,
    LabelProps {
  select?: CustomSelectProps;
  formName?: string | number | InternalNamePath | undefined;
}

const CustomInput: React.FC<CustomInputsProps> = ({
  formName,
  label,
  texttype,
  maxLength,
  formatter,
  onChange,
  select,
  rowGridWidth,
  ...rest
}) => {
  const showCount = label?.showCount;
  const [textLength, setTextLength] = useState(0);
  const [inputValue, setInputValue] = useState();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onCounting = (e: any) => {
    const { value } = e?.target;
    if (showCount) {
      setTextLength(value.length);
    }
    if (formatter && texttype === "number") {
      setInputValue(
        value.replace(/\$\s?|(,*)/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      );
    } else {
      setInputValue(value);
    }
    if (typeof onChange === "function") onChange(e);
  };

  const hasSelectInRow =
    (select && select?.options && select.options.length > 0) || false;

  return (
    <InputContainer>
      <Label
        {...label}
        showCount={showCount}
        counting={textLength}
        maxLength={maxLength}
        align={label?.labelAlign}
      />
      <RowContainer hasSelectInRow={hasSelectInRow} rowGridWidth={rowGridWidth}>
        {hasSelectInRow && (
          <FormItem name={`prefix_${formName}`}>
            <Select options={select?.options} hasSelectInRow={hasSelectInRow} />
          </FormItem>
        )}
        <Input
          {...rest}
          texttype={texttype}
          onChange={onCounting}
          value={inputValue}
          hasSelectInRow={hasSelectInRow}
        />
      </RowContainer>
    </InputContainer>
  );
};

interface CustomFromInputsProps
  extends CustomInputsProps,
    CustomFormItemsProps {
  helperText?: HelperProps;
}
const CustomFormInput: React.FC<CustomFromInputsProps> = ({
  formItem,
  helperText,
  ...rest
}) => {
  if (formItem)
    return (
      <>
        <FormItem {...formItem}>
          <CustomInput {...rest} formName={formItem.name} />
        </FormItem>
        {helperText && <HelperText {...helperText} />}
      </>
    );
  return <CustomInput {...rest} />;
};

export default CustomFormInput;
