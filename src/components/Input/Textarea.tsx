import React, { useState } from "react";
import styled from "styled-components";
import { Input as Inputtext } from "antd";
import { TextAreaProps } from "antd/lib/input";
import { InputContainer, Label, FormItem, HelperText } from "./index";
import { AlignProps } from "../../global/Interface.d";
import {
  CountingProps,
  LabelProps,
  CustomFormItemsProps,
  HelperProps,
} from "./InputInterface.d";

/* TEXT AREA */
const TextareaContainer = styled(
  ({ fullHeight, scrollable, height, expanded, align, ...rest }) => (
    <Inputtext.TextArea {...rest} />
  )
)<CustomTextareaProps>`
  border-radius: 4px !important;
  text-align: ${({ align }) => align || "left"};
  height: ${({ height, expanded }) =>
    !expanded && height ? `${height} !important` : "auto"};
  overflow: ${({ fullHeight, scrollable }) =>
    fullHeight && !scrollable ? "hidden !important" : "auto"};
`;
interface CustomTextareaProps extends TextAreaProps, AlignProps {
  height?: string;
  fullHeight?: boolean;
  scrollable?: boolean;
  expanded?: boolean | undefined;
}
const Textarea: React.FC<CustomTextareaProps> = ({
  fullHeight,
  scrollable,
  expanded,
  ...rest
}) => {
  return (
    <TextareaContainer
      {...rest}
      fullHeight={fullHeight}
      scrollable={scrollable}
      expanded={expanded}
      showCount={false}
      autoSize={fullHeight ? false : scrollable}
    />
  );
};

/* MAIN */
const RowContainer = styled.div`
  display: grid;
  grid-template-columns: 100%;
`;

interface CustomTextareasProps
  extends CustomTextareaProps,
    CountingProps,
    LabelProps {}

const CustomTextarea: React.FC<CustomTextareasProps> = ({
  label,
  maxLength,
  onChange,
  ...rest
}) => {
  const showCount = label?.showCount;
  const [textLength, setTextLength] = useState(0);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onCounting = (e: any) => {
    const { value } = e?.target;
    if (showCount) {
      setTextLength(value.length);
    }
    if (typeof onChange === "function") onChange(e);
  };

  return (
    <InputContainer>
      <Label
        {...label}
        showCount={showCount}
        counting={textLength}
        maxLength={maxLength}
        align={label?.labelAlign}
      />
      <RowContainer>
        <Textarea {...rest} onChange={onCounting} />
      </RowContainer>
    </InputContainer>
  );
};

interface CustomFromTextareaProps
  extends CustomTextareasProps,
    CustomFormItemsProps {
  helperText?: HelperProps;
}

const CustomFormTextarea: React.FC<CustomFromTextareaProps> = ({
  formItem,
  helperText,
  ...rest
}) => {
  if (formItem)
    return (
      <>
        <FormItem {...formItem}>
          <CustomTextarea {...rest} />
        </FormItem>
        {helperText && <HelperText {...helperText} />}
      </>
    );
  return <CustomTextarea {...rest} />;
};

export default CustomFormTextarea;
