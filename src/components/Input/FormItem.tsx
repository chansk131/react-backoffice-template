import React from "react";
import styled from "styled-components";
import { Form as FormDefault } from "antd";
import { CustomFormItemProps } from "./InputInterface.d";

const FormItemContainer = styled(FormDefault.Item)<CustomFormItemProps>`
  margin-bottom: 0px !important;
  padding: ${({ padding }) => padding || "0 0 0 0"};
  .ant-col.ant-form-item-control {
    text-align: ${({ align }) => align || "left"};
  }
  .ant-form-item-explain.ant-form-item-explain-error {
    font-size: 12px;
    line-height: 19px;
    color: #db302f;
    padding: 4px 0 0 0;
  }
`;

const FormItem: React.FC<CustomFormItemProps> = ({ children, ...rest }) => {
  return <FormItemContainer {...rest}>{children}</FormItemContainer>;
};

export default FormItem;
