import React from "react";
import { HelperProps } from "./InputInterface.d";
import { Typo } from "../Typo";

const HelperText: React.FC<HelperProps> = ({ text, padding, ...rest }) => {
  return (
    <Typo
      variant="span"
      color="#838e9a"
      fontFamily="BaiJamjuree"
      weight="normal"
      size="12px"
      {...rest}
      padding={padding || "4px 0"}
    >
      {text}
    </Typo>
  );
};

export default HelperText;
