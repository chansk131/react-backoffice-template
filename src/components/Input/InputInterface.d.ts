// import { FormProps } from "antd/lib/form/Form";
// import { FormProps } from "antd/lib/form";
import { FormItemProps } from "antd/lib/form";
import {
  TextStyleProps,
  AlignType,
  AlignProps,
} from "../../global/Interface.d";

/* DEFAULT */
export interface CountingProps {
  showCount?: boolean;
  maxLength?: number;
  counting?: number;
}
export interface hasSelectInRowProps {
  hasSelectInRow?: boolean;
}
export interface RowStyleProps extends hasSelectInRowProps {
  rowGridWidth?: string;
}

/* FORM */
export interface CustomFormItemProps extends AlignProps, FormItemProps {
  padding?: string;
}
export interface CustomFormItemsProps {
  formItem?: CustomFormItemProps;
}

/* LABEL */
export interface LabelTextProps
  extends TextStyleProps,
    AlignProps,
    CountingProps {
  text?: string;
  labelIconColor?: string;
  labelHint?: string;
  labelAlign?: AlignType;
  placement?:
    | "top"
    | "topLeft"
    | "topRight"
    | "left"
    | "leftTop"
    | "leftBottom"
    | "right"
    | "rightTop"
    | "rightBottom"
    | "bottom"
    | "bottomLeft"
    | "bottomRight";
}
export interface LabelProps {
  label?: LabelTextProps;
}

/* HELPER TEXT */
export interface HelperProps extends TextStyleProps, AlignProps {
  text?: string;
}

/* SELECT */
export interface CustomOptionsProps {
  text?: string;
  value: string;
}
export interface CustomSelectProps extends hasSelectInRowProps {
  defaultValue?: string;
  options?: Array<CustomOptionsProps>;
}
