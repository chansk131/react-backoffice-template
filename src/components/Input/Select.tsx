import React from "react";
import styled from "styled-components";
import { Select } from "antd";
import { CustomSelectProps } from "./InputInterface.d";

// import { OptionProps } from "antd/lib/select";

const { Option } = Select;

const SelectContianer = styled(({ hasSelectInRow, ...rest }) => (
  <Select {...rest} />
))<CustomSelectProps>`
  width: 100% !important;
  .ant-select-selector {
    border-radius: 4px !important;
    ${({ hasSelectInRow }) => {
      return `border-top-right-radius: 0px !important; 
                border-bottom-right-radius: 0px !important;`;
    }};
  }
`;

const CustomSelect: React.FC<CustomSelectProps> = ({
  defaultValue,
  options,
  ...props
}) => {
  if (options && options.length === 0) return null;
  return (
    <SelectContianer defaultValue={defaultValue} {...props}>
      {options &&
        options.map((option) => (
          <Option key={`options_${option.value}`} value={option.value}>
            {option?.text || option.value}
          </Option>
        ))}
    </SelectContianer>
  );
};

export default CustomSelect;
