import React from "react";
import { Form as FormDefault } from "antd";
import { FormProps } from "antd/lib/form";

const Form: React.FC<FormProps> = ({ children, ...rest }) => {
  return <FormDefault {...rest}>{children}</FormDefault>;
};

export default Form;
