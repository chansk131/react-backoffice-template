import React, { useState } from "react";
import dayjs from "dayjs";
import { RangeValue } from "rc-picker/es/interface";
import dayjsGenerateConfig from "rc-picker/lib/generate/dayjs";
import "antd/es/date-picker/style/index";
import generateRangePicker from "antd/es/date-picker/generatePicker/generateRangePicker";
import { RangePickerProps } from "antd/es/date-picker/generatePicker";
import styled from "styled-components";
import Color from "../../theme/color";

export type RangePickerProp = Omit<RangePickerProps<dayjs.Dayjs>, "picker">;

const Rp = generateRangePicker<dayjs.Dayjs>(dayjsGenerateConfig);

const FooterWrapper = styled.div`
  .ant-picker-ranges {
    padding: 4px 0;
  }
  .ant-picker-now-btn {
    padding: 4px 0;
    color: ${Color.primary.main};
    border: 1px solid ${Color.primary.main};
    box-sizing: border-box;
    border-radius: 4px;
    padding: 2px 10px;
    font-weight: 600;
    line-height: 20px;
    cursor: pointer;
  }

  .ant-picker-ok {
    button {
      background: #51cbce;
      border-radius: 4px;
      border-color: transparent;
      padding: 2px 10px;
      height: 1.7rem;
      width: auto;
      background: ${Color.primary.main};
    }
  }
`;

const StyledRangePicker = styled(Rp)<RangePickerProp>`
  width: 100%;
  .ant-picker-header-view button:hover {
    color: ${Color.primary.main};
  }
  .ant-picker-cell-range-start .ant-picker-cell-inner {
    color: #fff;
    background: ${Color.primary.main};
  }
  .ant-picker-cell-in-view {
    &.ant-picker-cell-selected .ant-picker-cell-inner,
    &.ant-picker-cell-range-end .ant-picker-cell-inner {
      background: ${Color.primary.main};
    }
    &.ant-picker-cell-in-range::before {
      background: ${Color.primary.lighter};
    }
    &.ant-picker-cell-today .ant-picker-cell-inner::before {
      border: 1px solid ${Color.primary.main};
    }
    &.ant-picker-cell-range-hover:not(.ant-picker-cell-in-range)::after {
      border-top: 1px dashed ${Color.primary.main};
      border-bottom: 1px dashed ${Color.primary.main};
    }
    &.ant-picker-cell-range-hover-start::after {
      border-left: 1px dashed ${Color.primary.main};
    }
    &.ant-picker-cell-range-hover-end::after {
      border-right: 1px dashed ${Color.primary.main};
    }
  }
`;

export interface CalendarInfo {
  range: string;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const RangePicker = React.forwardRef<any, RangePickerProp>((props, ref) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [selectedDate, setSelectedDate] = useState<any>([null, null]);

  const onCalendarChange = ({
    value,
    info,
  }: {
    value: RangeValue<dayjs.Dayjs>;
    info: CalendarInfo;
  }) => {
    const idx = info.range === "end" ? 1 : 0;
    const tmp = [...selectedDate];
    if (value) {
      tmp[idx] = value[idx];
      setSelectedDate(tmp);
    }
  };
  const onFocus = () => {
    if (!isOpen) {
      setIsOpen(true);
    }
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onBlur = (e: any) => {
    if (isOpen && !e.relatedTarget) {
      setTimeout(() => setIsOpen(false), 0);
    }
  };

  return (
    <StyledRangePicker
      {...props}
      ref={ref}
      onFocus={() => onFocus()}
      dropdownClassName="dp-calendar"
      renderExtraFooter={() => <Footer onOk={() => setIsOpen(false)} />}
      open={isOpen}
      onCalendarChange={(value, dateString, info) =>
        onCalendarChange({ value, info })
      }
      onChange={(val) => {
        if (!val) setSelectedDate([]);
      }}
      value={selectedDate}
      onBlur={(e) => onBlur(e)}
      getPopupContainer={(trigger) => trigger as HTMLElement}
    />
  );
});

export interface DatePickerFooterProp {
  onOk: React.MouseEventHandler<HTMLButtonElement>;
}

const Footer: React.FC<DatePickerFooterProp> = ({ onOk }) => {
  return (
    <FooterWrapper>
      <ul className="ant-picker-ranges">
        <li className="ant-picker-ok">
          <button
            type="button"
            className="ant-btn ant-btn-primary ant-btn-sm"
            onClick={(e) => onOk(e)}
          >
            <span>Ok</span>
          </button>
        </li>
      </ul>
    </FooterWrapper>
  );
};

export default RangePicker;
