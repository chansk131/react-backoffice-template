import { Dayjs } from "dayjs";
import * as React from "react";
import { PickerTimeProps } from "antd/es/date-picker/generatePicker";
import { Omit } from "antd/es/_util/type";
import DatePicker from "./DatePicker";

export type TimePickerProps = Omit<PickerTimeProps<Dayjs>, "picker">;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const TimePicker = React.forwardRef<any, TimePickerProps>((props, ref) => {
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <DatePicker {...props} mode={undefined} ref={ref} />;
});

TimePicker.displayName = "TimePicker";

export default TimePicker;
