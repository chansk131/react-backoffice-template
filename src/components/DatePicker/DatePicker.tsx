import React, { useState } from "react";
import dayjs from "dayjs";
import dayjsGenerateConfig from "rc-picker/lib/generate/dayjs";
import generatePicker, {
  PickerDateProps,
} from "antd/es/date-picker/generatePicker";
import "antd/es/date-picker/style/index";
import styled from "styled-components";
import Color from "../../theme/color";

export type PickerDateProp = Omit<PickerDateProps<dayjs.Dayjs>, "picker">;
const Dp = generatePicker<dayjs.Dayjs>(dayjsGenerateConfig);

const FooterWrapper = styled.div`
  .ant-picker-ranges {
    padding: 4px 0;
  }
  .ant-picker-now-btn {
    padding: 4px 0;
    color: ${Color.primary.main};
    border: 1px solid ${Color.primary.main};
    box-sizing: border-box;
    border-radius: 4px;
    padding: 2px 10px;
    font-weight: 600;
    line-height: 20px;
    cursor: pointer;
  }

  .ant-picker-ok {
    button {
      background: #51cbce;
      border-radius: 4px;
      border-color: transparent;
      padding: 2px 10px;
      height: 1.7rem;
      width: auto;
      background: ${Color.primary.main};
    }
  }
`;

const StyledDatePicker = styled(Dp)<PickerDateProp>`
  width: 100%;
  .ant-picker-header-view button:hover {
    color: ${Color.primary.main};
  }
  .ant-picker-cell-in-view.ant-picker-cell-selected .ant-picker-cell-inner {
    background: ${Color.primary.main};
  }
  .ant-picker-cell-in-view.ant-picker-cell-today
    .ant-picker-cell-inner::before {
    border: 1px solid ${Color.primary.main};
  }
`;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const DatePicker = React.forwardRef<any, PickerDateProp>((props, ref) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [selectedDate, setSelectedDate] = useState<dayjs.Dayjs | null>(null);
  return (
    <StyledDatePicker
      {...props}
      ref={ref}
      onFocus={() => setIsOpen(true)}
      dropdownClassName="dp-calendar"
      renderExtraFooter={() => (
        <Footer
          onOk={() => setIsOpen(false)}
          onToday={() => setSelectedDate(dayjs(new Date()))}
        />
      )}
      showToday={false}
      open={isOpen}
      onChange={(date) => setSelectedDate(date)}
      value={selectedDate}
      onBlur={() => setIsOpen(false)}
      getPopupContainer={(trigger) => trigger as HTMLElement}
    />
  );
});

export interface DatePickerFooterProp {
  onOk: React.MouseEventHandler<HTMLButtonElement>;
  onToday: React.MouseEventHandler<HTMLButtonElement>;
}

const Footer: React.FC<DatePickerFooterProp> = ({ onOk, onToday }) => {
  return (
    <FooterWrapper>
      <ul className="ant-picker-ranges">
        <li className="ant-picker-now">
          <button
            type="button"
            className="ant-picker-now-btn"
            onClick={(e) => onToday(e)}
          >
            Today
          </button>
        </li>
        <li className="ant-picker-ok">
          <button
            type="button"
            className="ant-btn ant-btn-primary ant-btn-sm"
            onClick={(e) => onOk(e)}
          >
            <span>Ok</span>
          </button>
        </li>
      </ul>
    </FooterWrapper>
  );
};

export default DatePicker;
