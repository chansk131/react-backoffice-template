import React, { ReactChild } from "react";
import styled, { css } from "styled-components";
import Color from "../../theme/color";

export interface TypographyProps {
  children?: ReactChild;
  variant?:
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "h5"
    | "h6"
    | "p"
    | "div"
    | "span"
    | undefined;
  align?: "inherit" | "left" | "center" | "right" | "justify";
  color?: string | undefined;
  display?: "initial" | "block" | "inline";
  whiteSpace?: "nowrap" | "normal" | "pre";
  wordBreak?:
    | "normal"
    | "break-all"
    | "keep-all"
    | "break-word"
    | "initial"
    | "inherit";
  underline?: boolean;
  size?: string | undefined;
  lineHeight?: string | undefined;
  weight?:
    | "normal"
    | "bolder"
    | "bold"
    | "lighter"
    | "100"
    | "200"
    | "300"
    | "400"
    | "500"
    | "600"
    | "700"
    | "800"
    | "900"
    | "initial"
    | "inherit";
  fontFamily?:
    | "IBMPlexSans"
    | "BaiJamjuree"
    | "IBMPlexMono"
    | "initial"
    | string
    | undefined;
  textOverflow?: number | false;
  padding?: string | undefined;
  margin?: string | undefined;
  marginBlockStart?: string;
  marginBlockEnd?: string;
  marginInlineStart?: string;
  marginInlineEnd?: string;
}

const styles = css<TypographyProps>`
  font-size: ${({ size }) => size || "inherit"};
  line-height: ${({ lineHeight }) => lineHeight || "inherit"};
  text-align: ${({ align }) => align || "inherit"};
  color: ${({ color }) => color};
  display: ${({ display }) => display || "block"};
  white-space: ${({ whiteSpace }) => whiteSpace || "normal"};
  word-break: ${({ wordBreak }) => wordBreak || "normal"};
  text-decoration: ${({ underline }) => (underline ? "underline" : "unset")};
  font-weight: ${({ weight }) => weight || "normal"};
  font-family: ${({ fontFamily }) => fontFamily || "inherit"};

  padding: ${({ padding }) => padding || "0 0 0 0"};
  margin: ${({ margin }) => margin || "0 0 0 0"};
  margin-block-start: ${({ marginBlockStart }) => marginBlockStart || "0px"};
  margin-block-end: ${({ marginBlockEnd }) => marginBlockEnd || "0px"};
  margin-inline-start: ${({ marginInlineStart }) => marginInlineStart || "0px"};
  margin-inline-end: ${({ marginInlineEnd }) => marginInlineEnd || "0px"};

  ${({ textOverflow }) =>
    textOverflow &&
    `
    overflow: hidden;
    -o-text-overflow: ellipsis;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: ${textOverflow};
    -webkit-box-orient: vertical;`}

  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select: none; /* Safari */
  -khtml-user-select: none; /* Konqueror HTML */
  -moz-user-select: none; /* Old versions of Firefox */
  -ms-user-select: none; /* Internet Explorer/Edge */
  user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome, Edge, Opera and Firefox */
`;
const H1Text = styled.h1<TypographyProps>`
  ${styles}
  font-size: ${({ size }) => size || "2em"};
  line-height: ${({ lineHeight }) => lineHeight || "2em"};
  font-weight: ${({ weight }: TypographyProps) => weight || "bold"};
  margin-block-start: ${({ marginBlockStart }) => marginBlockStart || "0.67em"};
  margin-block-end: ${({ marginBlockEnd }) => marginBlockEnd || "0.67em"};
  margin-inline-start: ${({ marginInlineStart }) => marginInlineStart || "0px"};
  margin-inline-end: ${({ marginInlineEnd }) => marginInlineEnd || "0px"};
`;
const H2Text = styled.h2<TypographyProps>`
  ${styles}
  font-size: ${({ size }) => size || "1.5em"};
  line-height: ${({ lineHeight }) => lineHeight || "1.5em"};
  font-weight: ${({ weight }: TypographyProps) => weight || "bold"};
  margin-block-start: ${({ marginBlockStart }) => marginBlockStart || "0.83em"};
  margin-block-end: ${({ marginBlockEnd }) => marginBlockEnd || "0.83em"};
  margin-inline-start: ${({ marginInlineStart }) => marginInlineStart || "0px"};
  margin-inline-end: ${({ marginInlineEnd }) => marginInlineEnd || "0px"};
`;
const H3Text = styled.h3<TypographyProps>`
  ${styles}
  font-size: ${({ size }) => size || "1.17em"};
  line-height: ${({ lineHeight }) => lineHeight || "1.17em"};
  font-weight: ${({ weight }: TypographyProps) => weight || "bold"};
  margin-block-start: ${({ marginBlockStart }) => marginBlockStart || "1em"};
  margin-block-end: ${({ marginBlockEnd }) => marginBlockEnd || "1em"};
  margin-inline-start: ${({ marginInlineStart }) => marginInlineStart || "0px"};
  margin-inline-end: ${({ marginInlineEnd }) => marginInlineEnd || "0px"};
`;
const H4Text = styled.h4<TypographyProps>`
  ${styles}
  font-size: ${({ size }) => size || "1.33em"};
  line-height: ${({ lineHeight }) => lineHeight || "1.33em"};
  font-weight: ${({ weight }: TypographyProps) => weight || "bold"};
  margin-block-start: ${({ marginBlockStart }) => marginBlockStart || "1.33em"};
  margin-block-end: ${({ marginBlockEnd }) => marginBlockEnd || "1.33em"};
  margin-inline-start: ${({ marginInlineStart }) => marginInlineStart || "0px"};
  margin-inline-end: ${({ marginInlineEnd }) => marginInlineEnd || "0px"};
`;
const H5Text = styled.h5<TypographyProps>`
  ${styles}
  font-size: ${({ size }) => size || "0.83em"};
  line-height: ${({ lineHeight }) => lineHeight || "0.83em"};
  font-weight: ${({ weight }: TypographyProps) => weight || "bold"};
  margin-block-start: ${({ marginBlockStart }) => marginBlockStart || "1.67em"};
  margin-block-end: ${({ marginBlockEnd }) => marginBlockEnd || "1.67em"};
  margin-inline-start: ${({ marginInlineStart }) => marginInlineStart || "0px"};
  margin-inline-end: ${({ marginInlineEnd }) => marginInlineEnd || "0px"};
`;
const H6Text = styled.h6<TypographyProps>`
  ${styles}
  font-size: ${({ size }) => size || "0.67em"};
  line-height: ${({ lineHeight }) => lineHeight || "0.67em"};
  font-weight: ${({ weight }: TypographyProps) => weight || "bold"};
  margin-block-start: ${({ marginBlockStart }) => marginBlockStart || "2.33em"};
  margin-block-end: ${({ marginBlockEnd }) => marginBlockEnd || "2.33em"};
  margin-inline-start: ${({ marginInlineStart }) => marginInlineStart || "0px"};
  margin-inline-end: ${({ marginInlineEnd }) => marginInlineEnd || "0px"};
`;
const PText = styled.p<TypographyProps>`
  ${styles}
  margin-block-start: ${({ marginBlockStart }) => marginBlockStart || "1em"};
  margin-block-end: ${({ marginBlockEnd }) => marginBlockEnd || "1em"};
  margin-inline-start: ${({ marginInlineStart }) => marginInlineStart || "0px"};
  margin-inline-end: ${({ marginInlineEnd }) => marginInlineEnd || "0px"};
`;
const SpanText = styled.span<TypographyProps>`
  ${styles}
`;
const DivText = styled.div<TypographyProps>`
  ${styles}
`;

const Typography: React.FC<TypographyProps> = ({
  children,
  variant,
  color = "primary",
  ...props
}) => {
  const colorHexCode = color
    ? Color[color]?.main || Color[color] || color
    : "#000000";

  if (variant === "div")
    return (
      <DivText color={colorHexCode} {...props}>
        {children}
      </DivText>
    );
  if (variant === "span")
    return (
      <SpanText color={colorHexCode} {...props}>
        {children}
      </SpanText>
    );
  if (variant === "p")
    return (
      <PText color={colorHexCode} {...props}>
        {children}
      </PText>
    );
  if (variant === "h1")
    return (
      <H1Text color={colorHexCode} {...props}>
        {children}
      </H1Text>
    );
  if (variant === "h2")
    return (
      <H2Text color={colorHexCode} {...props}>
        {children}
      </H2Text>
    );
  if (variant === "h3")
    return (
      <H3Text color={colorHexCode} {...props}>
        {children}
      </H3Text>
    );
  if (variant === "h4")
    return (
      <H4Text color={colorHexCode} {...props}>
        {children}
      </H4Text>
    );
  if (variant === "h5")
    return (
      <H5Text color={colorHexCode} {...props}>
        {children}
      </H5Text>
    );
  if (variant === "h6")
    return (
      <H6Text color={colorHexCode} {...props}>
        {children}
      </H6Text>
    );

  return <>{children}</>;
};

export default Typography;
