import React from "react";
import { render, screen } from "@testing-library/react";
import "jest-styled-components";
// import "@testing-library/jest-dom/extend-expect";
import Typo from "../Typo";

describe("Default", () => {
  test("text", () => {
    render(<Typo>text</Typo>);
    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();
  });
});

describe("Div", () => {
  test("default", () => {
    const { container } = render(<Typo variant="div">text</Typo>);

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("div");
    expect(elements).toHaveStyleRule("font-size", "inherit");
    expect(elements).toHaveStyleRule("line-height", "inherit");
    expect(elements).toHaveStyleRule("text-align", "inherit");
    expect(elements).toHaveStyleRule("color", "#51cbce");
    expect(elements).toHaveStyleRule("display", "block");
    expect(elements).toHaveStyleRule("white-space", "normal");
    expect(elements).toHaveStyleRule("word-break", "normal");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "normal");
    expect(elements).toHaveStyleRule("font-family", "inherit");
    expect(elements).toHaveStyleRule("padding", "0 0 0 0");
    expect(elements).toHaveStyleRule("margin", "0 0 0 0");

    expect(elements).toHaveStyleRule("margin-block-start", "0px");
    expect(elements).toHaveStyleRule("margin-block-end", "0px");
    expect(elements).toHaveStyleRule("margin-inline-start", "0px");
    expect(elements).toHaveStyleRule("margin-inline-end", "0px");
  });

  test("with prop", () => {
    const { container } = render(
      <Typo
        variant="div"
        size="14px"
        lineHeight="16px"
        align="justify"
        color="#3d3d3d"
        display="initial"
        whiteSpace="pre"
        wordBreak="keep-all"
        weight="bold"
        fontFamily="BaiJamjuree"
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("div");
    expect(elements).toHaveStyleRule("font-size", "14px");
    expect(elements).toHaveStyleRule("line-height", "16px");
    expect(elements).toHaveStyleRule("text-align", "justify");
    expect(elements).toHaveStyleRule("color", "#3d3d3d");
    expect(elements).toHaveStyleRule("display", "initial");
    expect(elements).toHaveStyleRule("white-space", "pre");
    expect(elements).toHaveStyleRule("word-break", "keep-all");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "bold");
    expect(elements).toHaveStyleRule("font-family", "BaiJamjuree");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");
  });

  test("with prop, fix with 2 lines and underline", () => {
    const { container } = render(
      <Typo
        variant="div"
        size="12px"
        lineHeight="14px"
        align="left"
        color="default"
        display="initial"
        whiteSpace="nowrap"
        wordBreak="break-all"
        underline
        weight="bolder"
        fontFamily="IBMPlexSans"
        textOverflow={2}
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("div");
    expect(elements).toHaveStyleRule("font-size", "12px");
    expect(elements).toHaveStyleRule("line-height", "14px");
    expect(elements).toHaveStyleRule("text-align", "left");
    expect(elements).toHaveStyleRule("color", "#505B67");
    expect(elements).toHaveStyleRule("white-space", "nowrap");
    expect(elements).toHaveStyleRule("word-break", "break-all");
    expect(elements).toHaveStyleRule("text-decoration", "underline");
    expect(elements).toHaveStyleRule("font-weight", "bolder");
    expect(elements).toHaveStyleRule("font-family", "IBMPlexSans");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");

    expect(elements).toHaveStyleRule("overflow", "hidden");
    expect(elements).toHaveStyleRule("-o-text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("display", "-webkit-box");
    expect(elements).toHaveStyleRule("-webkit-line-clamp", "2");
    expect(elements).toHaveStyleRule("-webkit-box-orient", "vertical");
  });
});

describe("Span", () => {
  test("default", () => {
    const { container } = render(<Typo variant="span">text</Typo>);

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("span");
    expect(elements).toHaveStyleRule("font-size", "inherit");
    expect(elements).toHaveStyleRule("line-height", "inherit");
    expect(elements).toHaveStyleRule("text-align", "inherit");
    expect(elements).toHaveStyleRule("color", "#51cbce");
    expect(elements).toHaveStyleRule("display", "block");
    expect(elements).toHaveStyleRule("white-space", "normal");
    expect(elements).toHaveStyleRule("word-break", "normal");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "normal");
    expect(elements).toHaveStyleRule("font-family", "inherit");
    expect(elements).toHaveStyleRule("padding", "0 0 0 0");
    expect(elements).toHaveStyleRule("margin", "0 0 0 0");

    expect(elements).toHaveStyleRule("margin-block-start", "0px");
    expect(elements).toHaveStyleRule("margin-block-end", "0px");
    expect(elements).toHaveStyleRule("margin-inline-start", "0px");
    expect(elements).toHaveStyleRule("margin-inline-end", "0px");
  });

  test("with prop", () => {
    const { container } = render(
      <Typo
        variant="span"
        size="14px"
        lineHeight="16px"
        align="right"
        color="white"
        display="inline"
        whiteSpace="pre"
        wordBreak="break-word"
        weight="lighter"
        fontFamily="IBMPlexMono"
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("span");
    expect(elements).toHaveStyleRule("font-size", "14px");
    expect(elements).toHaveStyleRule("line-height", "16px");
    expect(elements).toHaveStyleRule("text-align", "right");
    expect(elements).toHaveStyleRule("color", "#ffffff");
    expect(elements).toHaveStyleRule("display", "inline");
    expect(elements).toHaveStyleRule("white-space", "pre");
    expect(elements).toHaveStyleRule("word-break", "break-word");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "lighter");
    expect(elements).toHaveStyleRule("font-family", "IBMPlexMono");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");
  });

  test("with prop, fix with 2 lines and underline", () => {
    const { container } = render(
      <Typo
        variant="span"
        size="12px"
        lineHeight="14px"
        align="left"
        color="default"
        display="initial"
        whiteSpace="nowrap"
        wordBreak="break-word"
        underline
        weight="100"
        fontFamily="initial"
        textOverflow={2}
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("span");
    expect(elements).toHaveStyleRule("font-size", "12px");
    expect(elements).toHaveStyleRule("line-height", "14px");
    expect(elements).toHaveStyleRule("text-align", "left");
    expect(elements).toHaveStyleRule("color", "#505B67");
    expect(elements).toHaveStyleRule("white-space", "nowrap");
    expect(elements).toHaveStyleRule("word-break", "break-word");
    expect(elements).toHaveStyleRule("text-decoration", "underline");
    expect(elements).toHaveStyleRule("font-weight", "100");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");

    expect(elements).toHaveStyleRule("overflow", "hidden");
    expect(elements).toHaveStyleRule("-o-text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("display", "-webkit-box");
    expect(elements).toHaveStyleRule("-webkit-line-clamp", "2");
    expect(elements).toHaveStyleRule("-webkit-box-orient", "vertical");
  });
});

describe("P", () => {
  test("default", () => {
    const { container } = render(<Typo variant="p">text</Typo>);

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("p");
    expect(elements).toHaveStyleRule("font-size", "inherit");
    expect(elements).toHaveStyleRule("line-height", "inherit");
    expect(elements).toHaveStyleRule("text-align", "inherit");
    expect(elements).toHaveStyleRule("color", "#51cbce");
    expect(elements).toHaveStyleRule("display", "block");
    expect(elements).toHaveStyleRule("white-space", "normal");
    expect(elements).toHaveStyleRule("word-break", "normal");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "normal");
    expect(elements).toHaveStyleRule("font-family", "inherit");
    expect(elements).toHaveStyleRule("padding", "0 0 0 0");
    expect(elements).toHaveStyleRule("margin", "0 0 0 0");

    expect(elements).toHaveStyleRule("margin-block-start", "1em");
    expect(elements).toHaveStyleRule("margin-block-end", "1em");
    expect(elements).toHaveStyleRule("margin-inline-start", "0px");
    expect(elements).toHaveStyleRule("margin-inline-end", "0px");
  });

  test("with prop", () => {
    const { container } = render(
      <Typo
        variant="p"
        size="14px"
        lineHeight="16px"
        align="right"
        color="white"
        display="inline"
        whiteSpace="pre"
        wordBreak="break-word"
        weight="lighter"
        fontFamily="IBMPlexMono"
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("p");
    expect(elements).toHaveStyleRule("font-size", "14px");
    expect(elements).toHaveStyleRule("line-height", "16px");
    expect(elements).toHaveStyleRule("text-align", "right");
    expect(elements).toHaveStyleRule("color", "#ffffff");
    expect(elements).toHaveStyleRule("display", "inline");
    expect(elements).toHaveStyleRule("white-space", "pre");
    expect(elements).toHaveStyleRule("word-break", "break-word");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "lighter");
    expect(elements).toHaveStyleRule("font-family", "IBMPlexMono");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");
  });

  test("with prop, fix with 2 lines and underline", () => {
    const { container } = render(
      <Typo
        variant="p"
        size="12px"
        lineHeight="14px"
        align="left"
        display="initial"
        whiteSpace="nowrap"
        wordBreak="break-word"
        underline
        weight="100"
        fontFamily="initial"
        textOverflow={2}
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
        color={null}
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("p");
    expect(elements).toHaveStyleRule("font-size", "12px");
    expect(elements).toHaveStyleRule("line-height", "14px");
    expect(elements).toHaveStyleRule("text-align", "left");
    expect(elements).toHaveStyleRule("color", "#000000");
    expect(elements).toHaveStyleRule("white-space", "nowrap");
    expect(elements).toHaveStyleRule("word-break", "break-word");
    expect(elements).toHaveStyleRule("text-decoration", "underline");
    expect(elements).toHaveStyleRule("font-weight", "100");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");

    expect(elements).toHaveStyleRule("overflow", "hidden");
    expect(elements).toHaveStyleRule("-o-text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("display", "-webkit-box");
    expect(elements).toHaveStyleRule("-webkit-line-clamp", "2");
    expect(elements).toHaveStyleRule("-webkit-box-orient", "vertical");
  });
});

describe("H1", () => {
  test("default", () => {
    const { container } = render(<Typo variant="h1">text</Typo>);

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h1");
    expect(elements).toHaveStyleRule("font-size", "2em");
    expect(elements).toHaveStyleRule("line-height", "2em");
    expect(elements).toHaveStyleRule("text-align", "inherit");
    expect(elements).toHaveStyleRule("color", "#51cbce");
    expect(elements).toHaveStyleRule("display", "block");
    expect(elements).toHaveStyleRule("white-space", "normal");
    expect(elements).toHaveStyleRule("word-break", "normal");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "bold");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "0 0 0 0");
    expect(elements).toHaveStyleRule("margin", "0 0 0 0");

    expect(elements).toHaveStyleRule("margin-block-start", "0.67em");
    expect(elements).toHaveStyleRule("margin-block-end", "0.67em");
    expect(elements).toHaveStyleRule("margin-inline-start", "0px");
    expect(elements).toHaveStyleRule("margin-inline-end", "0px");
  });

  test("with prop", () => {
    const { container } = render(
      <Typo
        variant="h1"
        size="14px"
        lineHeight="16px"
        align="right"
        color="black"
        display="inline"
        whiteSpace="pre"
        wordBreak="inherit"
        weight="200"
        fontFamily="IBMPlexMono"
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h1");
    expect(elements).toHaveStyleRule("font-size", "14px");
    expect(elements).toHaveStyleRule("line-height", "16px");
    expect(elements).toHaveStyleRule("text-align", "right");
    expect(elements).toHaveStyleRule("color", "black");
    expect(elements).toHaveStyleRule("display", "inline");
    expect(elements).toHaveStyleRule("white-space", "pre");
    expect(elements).toHaveStyleRule("word-break", "inherit");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "200");
    expect(elements).toHaveStyleRule("font-family", "IBMPlexMono");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");
  });

  test("with prop, fix with 2 lines and underline", () => {
    const { container } = render(
      <Typo
        variant="h1"
        size="12px"
        lineHeight="14px"
        align="left"
        color="error"
        display="initial"
        whiteSpace="nowrap"
        wordBreak="break-word"
        underline
        weight="300"
        fontFamily="initial"
        textOverflow={2}
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h1");
    expect(elements).toHaveStyleRule("font-size", "12px");
    expect(elements).toHaveStyleRule("line-height", "14px");
    expect(elements).toHaveStyleRule("text-align", "left");
    expect(elements).toHaveStyleRule("color", "#db302f");
    expect(elements).toHaveStyleRule("white-space", "nowrap");
    expect(elements).toHaveStyleRule("word-break", "break-word");
    expect(elements).toHaveStyleRule("text-decoration", "underline");
    expect(elements).toHaveStyleRule("font-weight", "300");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");

    expect(elements).toHaveStyleRule("overflow", "hidden");
    expect(elements).toHaveStyleRule("-o-text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("display", "-webkit-box");
    expect(elements).toHaveStyleRule("-webkit-line-clamp", "2");
    expect(elements).toHaveStyleRule("-webkit-box-orient", "vertical");
  });
});

describe("H2", () => {
  test("default", () => {
    const { container } = render(<Typo variant="h2">text</Typo>);

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h2");
    expect(elements).toHaveStyleRule("font-size", "1.5em");
    expect(elements).toHaveStyleRule("line-height", "1.5em");
    expect(elements).toHaveStyleRule("text-align", "inherit");
    expect(elements).toHaveStyleRule("color", "#51cbce");
    expect(elements).toHaveStyleRule("display", "block");
    expect(elements).toHaveStyleRule("white-space", "normal");
    expect(elements).toHaveStyleRule("word-break", "normal");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "bold");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "0 0 0 0");
    expect(elements).toHaveStyleRule("margin", "0 0 0 0");

    expect(elements).toHaveStyleRule("margin-block-start", "0.83em");
    expect(elements).toHaveStyleRule("margin-block-end", "0.83em");
    expect(elements).toHaveStyleRule("margin-inline-start", "0px");
    expect(elements).toHaveStyleRule("margin-inline-end", "0px");
  });

  test("with prop", () => {
    const { container } = render(
      <Typo
        variant="h2"
        size="14px"
        lineHeight="16px"
        align="right"
        color="black"
        display="inline"
        whiteSpace="pre"
        wordBreak="inherit"
        weight="400"
        fontFamily="IBMPlexMono"
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h2");
    expect(elements).toHaveStyleRule("font-size", "14px");
    expect(elements).toHaveStyleRule("line-height", "16px");
    expect(elements).toHaveStyleRule("text-align", "right");
    expect(elements).toHaveStyleRule("color", "black");
    expect(elements).toHaveStyleRule("display", "inline");
    expect(elements).toHaveStyleRule("white-space", "pre");
    expect(elements).toHaveStyleRule("word-break", "inherit");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "400");
    expect(elements).toHaveStyleRule("font-family", "IBMPlexMono");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");
  });

  test("with prop, fix with 2 lines and underline", () => {
    const { container } = render(
      <Typo
        variant="h2"
        size="12px"
        lineHeight="14px"
        align="left"
        color="error"
        display="initial"
        whiteSpace="nowrap"
        wordBreak="break-word"
        underline
        weight="500"
        fontFamily="initial"
        textOverflow={2}
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h2");
    expect(elements).toHaveStyleRule("font-size", "12px");
    expect(elements).toHaveStyleRule("line-height", "14px");
    expect(elements).toHaveStyleRule("text-align", "left");
    expect(elements).toHaveStyleRule("color", "#db302f");
    expect(elements).toHaveStyleRule("white-space", "nowrap");
    expect(elements).toHaveStyleRule("word-break", "break-word");
    expect(elements).toHaveStyleRule("text-decoration", "underline");
    expect(elements).toHaveStyleRule("font-weight", "500");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");

    expect(elements).toHaveStyleRule("overflow", "hidden");
    expect(elements).toHaveStyleRule("-o-text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("display", "-webkit-box");
    expect(elements).toHaveStyleRule("-webkit-line-clamp", "2");
    expect(elements).toHaveStyleRule("-webkit-box-orient", "vertical");
  });
});

describe("H3", () => {
  test("default", () => {
    const { container } = render(<Typo variant="h3">text</Typo>);

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h3");
    expect(elements).toHaveStyleRule("font-size", "1.17em");
    expect(elements).toHaveStyleRule("line-height", "1.17em");
    expect(elements).toHaveStyleRule("text-align", "inherit");
    expect(elements).toHaveStyleRule("color", "#51cbce");
    expect(elements).toHaveStyleRule("display", "block");
    expect(elements).toHaveStyleRule("white-space", "normal");
    expect(elements).toHaveStyleRule("word-break", "normal");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "bold");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "0 0 0 0");
    expect(elements).toHaveStyleRule("margin", "0 0 0 0");

    expect(elements).toHaveStyleRule("margin-block-start", "1em");
    expect(elements).toHaveStyleRule("margin-block-end", "1em");
    expect(elements).toHaveStyleRule("margin-inline-start", "0px");
    expect(elements).toHaveStyleRule("margin-inline-end", "0px");
  });

  test("with prop", () => {
    const { container } = render(
      <Typo
        variant="h3"
        size="14px"
        lineHeight="16px"
        align="right"
        color="black"
        display="inline"
        whiteSpace="pre"
        wordBreak="inherit"
        weight="600"
        fontFamily="IBMPlexMono"
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h3");
    expect(elements).toHaveStyleRule("font-size", "14px");
    expect(elements).toHaveStyleRule("line-height", "16px");
    expect(elements).toHaveStyleRule("text-align", "right");
    expect(elements).toHaveStyleRule("color", "black");
    expect(elements).toHaveStyleRule("display", "inline");
    expect(elements).toHaveStyleRule("white-space", "pre");
    expect(elements).toHaveStyleRule("word-break", "inherit");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "600");
    expect(elements).toHaveStyleRule("font-family", "IBMPlexMono");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");
  });

  test("with prop, fix with 2 lines and underline", () => {
    const { container } = render(
      <Typo
        variant="h3"
        size="12px"
        lineHeight="14px"
        align="left"
        color="error"
        display="initial"
        whiteSpace="nowrap"
        wordBreak="break-word"
        underline
        weight="700"
        fontFamily="initial"
        textOverflow={2}
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h3");
    expect(elements).toHaveStyleRule("font-size", "12px");
    expect(elements).toHaveStyleRule("line-height", "14px");
    expect(elements).toHaveStyleRule("text-align", "left");
    expect(elements).toHaveStyleRule("color", "#db302f");
    expect(elements).toHaveStyleRule("white-space", "nowrap");
    expect(elements).toHaveStyleRule("word-break", "break-word");
    expect(elements).toHaveStyleRule("text-decoration", "underline");
    expect(elements).toHaveStyleRule("font-weight", "700");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");

    expect(elements).toHaveStyleRule("overflow", "hidden");
    expect(elements).toHaveStyleRule("-o-text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("display", "-webkit-box");
    expect(elements).toHaveStyleRule("-webkit-line-clamp", "2");
    expect(elements).toHaveStyleRule("-webkit-box-orient", "vertical");
  });
});

describe("H4", () => {
  test("default", () => {
    const { container } = render(<Typo variant="h4">text</Typo>);

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h4");
    expect(elements).toHaveStyleRule("font-size", "1.33em");
    expect(elements).toHaveStyleRule("line-height", "1.33em");
    expect(elements).toHaveStyleRule("text-align", "inherit");
    expect(elements).toHaveStyleRule("color", "#51cbce");
    expect(elements).toHaveStyleRule("display", "block");
    expect(elements).toHaveStyleRule("white-space", "normal");
    expect(elements).toHaveStyleRule("word-break", "normal");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "bold");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "0 0 0 0");
    expect(elements).toHaveStyleRule("margin", "0 0 0 0");

    expect(elements).toHaveStyleRule("margin-block-start", "1.33em");
    expect(elements).toHaveStyleRule("margin-block-end", "1.33em");
    expect(elements).toHaveStyleRule("margin-inline-start", "0px");
    expect(elements).toHaveStyleRule("margin-inline-end", "0px");
  });

  test("with prop", () => {
    const { container } = render(
      <Typo
        variant="h4"
        size="14px"
        lineHeight="16px"
        align="right"
        color="black"
        display="inline"
        whiteSpace="pre"
        wordBreak="inherit"
        weight="800"
        fontFamily="IBMPlexMono"
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h4");
    expect(elements).toHaveStyleRule("font-size", "14px");
    expect(elements).toHaveStyleRule("line-height", "16px");
    expect(elements).toHaveStyleRule("text-align", "right");
    expect(elements).toHaveStyleRule("color", "black");
    expect(elements).toHaveStyleRule("display", "inline");
    expect(elements).toHaveStyleRule("white-space", "pre");
    expect(elements).toHaveStyleRule("word-break", "inherit");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "800");
    expect(elements).toHaveStyleRule("font-family", "IBMPlexMono");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");
  });

  test("with prop, fix with 2 lines and underline", () => {
    const { container } = render(
      <Typo
        variant="h4"
        size="12px"
        lineHeight="14px"
        align="left"
        color="error"
        display="initial"
        whiteSpace="nowrap"
        wordBreak="break-word"
        underline
        weight="900"
        fontFamily="initial"
        textOverflow={2}
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h4");
    expect(elements).toHaveStyleRule("font-size", "12px");
    expect(elements).toHaveStyleRule("line-height", "14px");
    expect(elements).toHaveStyleRule("text-align", "left");
    expect(elements).toHaveStyleRule("color", "#db302f");
    expect(elements).toHaveStyleRule("white-space", "nowrap");
    expect(elements).toHaveStyleRule("word-break", "break-word");
    expect(elements).toHaveStyleRule("text-decoration", "underline");
    expect(elements).toHaveStyleRule("font-weight", "900");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");

    expect(elements).toHaveStyleRule("overflow", "hidden");
    expect(elements).toHaveStyleRule("-o-text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("display", "-webkit-box");
    expect(elements).toHaveStyleRule("-webkit-line-clamp", "2");
    expect(elements).toHaveStyleRule("-webkit-box-orient", "vertical");
  });
});

describe("H5", () => {
  test("default", () => {
    const { container } = render(<Typo variant="h5">text</Typo>);

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h5");
    expect(elements).toHaveStyleRule("font-size", "0.83em");
    expect(elements).toHaveStyleRule("line-height", "0.83em");
    expect(elements).toHaveStyleRule("text-align", "inherit");
    expect(elements).toHaveStyleRule("color", "#51cbce");
    expect(elements).toHaveStyleRule("display", "block");
    expect(elements).toHaveStyleRule("white-space", "normal");
    expect(elements).toHaveStyleRule("word-break", "normal");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "bold");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "0 0 0 0");
    expect(elements).toHaveStyleRule("margin", "0 0 0 0");

    expect(elements).toHaveStyleRule("margin-block-start", "1.67em");
    expect(elements).toHaveStyleRule("margin-block-end", "1.67em");
    expect(elements).toHaveStyleRule("margin-inline-start", "0px");
    expect(elements).toHaveStyleRule("margin-inline-end", "0px");
  });

  test("with prop", () => {
    const { container } = render(
      <Typo
        variant="h5"
        size="14px"
        lineHeight="16px"
        align="right"
        color="black"
        display="inline"
        whiteSpace="pre"
        wordBreak="inherit"
        weight="lighter"
        fontFamily="IBMPlexMono"
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h5");
    expect(elements).toHaveStyleRule("font-size", "14px");
    expect(elements).toHaveStyleRule("line-height", "16px");
    expect(elements).toHaveStyleRule("text-align", "right");
    expect(elements).toHaveStyleRule("color", "black");
    expect(elements).toHaveStyleRule("display", "inline");
    expect(elements).toHaveStyleRule("white-space", "pre");
    expect(elements).toHaveStyleRule("word-break", "inherit");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "lighter");
    expect(elements).toHaveStyleRule("font-family", "IBMPlexMono");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");
  });

  test("with prop, fix with 2 lines and underline", () => {
    const { container } = render(
      <Typo
        variant="h5"
        size="12px"
        lineHeight="14px"
        align="left"
        color="error"
        display="initial"
        whiteSpace="nowrap"
        wordBreak="break-word"
        underline
        weight="bolder"
        fontFamily="initial"
        textOverflow={2}
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h5");
    expect(elements).toHaveStyleRule("font-size", "12px");
    expect(elements).toHaveStyleRule("line-height", "14px");
    expect(elements).toHaveStyleRule("text-align", "left");
    expect(elements).toHaveStyleRule("color", "#db302f");
    expect(elements).toHaveStyleRule("white-space", "nowrap");
    expect(elements).toHaveStyleRule("word-break", "break-word");
    expect(elements).toHaveStyleRule("text-decoration", "underline");
    expect(elements).toHaveStyleRule("font-weight", "bolder");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");

    expect(elements).toHaveStyleRule("overflow", "hidden");
    expect(elements).toHaveStyleRule("-o-text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("display", "-webkit-box");
    expect(elements).toHaveStyleRule("-webkit-line-clamp", "2");
    expect(elements).toHaveStyleRule("-webkit-box-orient", "vertical");
  });
});

describe("H6", () => {
  test("default", () => {
    const { container } = render(<Typo variant="h6">text</Typo>);

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h6");
    expect(elements).toHaveStyleRule("font-size", "0.67em");
    expect(elements).toHaveStyleRule("line-height", "0.67em");
    expect(elements).toHaveStyleRule("text-align", "inherit");
    expect(elements).toHaveStyleRule("color", "#51cbce");
    expect(elements).toHaveStyleRule("display", "block");
    expect(elements).toHaveStyleRule("white-space", "normal");
    expect(elements).toHaveStyleRule("word-break", "normal");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "bold");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "0 0 0 0");
    expect(elements).toHaveStyleRule("margin", "0 0 0 0");

    expect(elements).toHaveStyleRule("margin-block-start", "2.33em");
    expect(elements).toHaveStyleRule("margin-block-end", "2.33em");
    expect(elements).toHaveStyleRule("margin-inline-start", "0px");
    expect(elements).toHaveStyleRule("margin-inline-end", "0px");
  });

  test("with prop", () => {
    const { container } = render(
      <Typo
        variant="h6"
        size="14px"
        lineHeight="16px"
        align="right"
        color="black"
        display="inline"
        whiteSpace="pre"
        wordBreak="inherit"
        weight="lighter"
        fontFamily="IBMPlexMono"
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h6");
    expect(elements).toHaveStyleRule("font-size", "14px");
    expect(elements).toHaveStyleRule("line-height", "16px");
    expect(elements).toHaveStyleRule("text-align", "right");
    expect(elements).toHaveStyleRule("color", "black");
    expect(elements).toHaveStyleRule("display", "inline");
    expect(elements).toHaveStyleRule("white-space", "pre");
    expect(elements).toHaveStyleRule("word-break", "inherit");
    expect(elements).toHaveStyleRule("text-decoration", "unset");
    expect(elements).toHaveStyleRule("font-weight", "lighter");
    expect(elements).toHaveStyleRule("font-family", "IBMPlexMono");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");
  });

  test("with prop, fix with 2 lines and underline", () => {
    const { container } = render(
      <Typo
        variant="h6"
        size="12px"
        lineHeight="14px"
        align="left"
        color="error"
        display="initial"
        whiteSpace="nowrap"
        wordBreak="break-word"
        underline
        weight="bolder"
        fontFamily="initial"
        textOverflow={2}
        padding={"10px 10px 10px 10px"}
        margin={"10px 10px 10px 10px"}
        marginBlockStart="10px"
        marginBlockEnd="10px"
        marginInlineStart="10px"
        marginInlineEnd="10px"
      >
        text
      </Typo>
    );

    const textElement = screen.getByText(/text/i);
    expect(textElement).toBeInTheDocument();

    const elements = container.querySelector("h6");
    expect(elements).toHaveStyleRule("font-size", "12px");
    expect(elements).toHaveStyleRule("line-height", "14px");
    expect(elements).toHaveStyleRule("text-align", "left");
    expect(elements).toHaveStyleRule("color", "#db302f");
    expect(elements).toHaveStyleRule("white-space", "nowrap");
    expect(elements).toHaveStyleRule("word-break", "break-word");
    expect(elements).toHaveStyleRule("text-decoration", "underline");
    expect(elements).toHaveStyleRule("font-weight", "bolder");
    expect(elements).toHaveStyleRule("font-family", "initial");
    expect(elements).toHaveStyleRule("padding", "10px 10px 10px 10px");
    expect(elements).toHaveStyleRule("margin", "10px 10px 10px 10px");

    expect(elements).toHaveStyleRule("margin-block-start", "10px");
    expect(elements).toHaveStyleRule("margin-block-end", "10px");
    expect(elements).toHaveStyleRule("margin-inline-start", "10px");
    expect(elements).toHaveStyleRule("margin-inline-end", "10px");

    expect(elements).toHaveStyleRule("overflow", "hidden");
    expect(elements).toHaveStyleRule("-o-text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("text-overflow", "ellipsis");
    expect(elements).toHaveStyleRule("display", "-webkit-box");
    expect(elements).toHaveStyleRule("-webkit-line-clamp", "2");
    expect(elements).toHaveStyleRule("-webkit-box-orient", "vertical");
  });
});
