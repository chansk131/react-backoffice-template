export interface TextStyleProps {
  size?: string;
  lineHeight?: string;
  color?: string;
  padding?: string;
}

export type AlignType = "left" | "center" | "right";
export interface AlignProps {
  align?: AlignType;
}
