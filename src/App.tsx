import React, { lazy, Suspense, useEffect } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import PageWrapper from "./components/layout/PageWrapper";
import PrivateRoute from "./PrivateRoute";
import useAuth from "./services/auth-hook";

const LoginPage = lazy(() => import("./pages/LoginPage"));
const AboutPage = lazy(() => import("./pages/AboutPage"));
const ButtonPage = lazy(() => import("./pages/ButtonPage"));
const TypoPage = lazy(() => import("./pages/TypoPage"));
const InputPage = lazy(() => import("./pages/InputPage"));
const TagPage = lazy(() => import("./pages/TagPage"));
const ModalPage = lazy(() => import("./pages/ModalPage"));

function App(): JSX.Element {
  const { refresh } = useAuth();

  // refresh the first time running the app
  // if 403 then redirect to login page
  // else go to authorised page
  useEffect(() => {
    refresh();
  }, [refresh]);

  return (
    <BrowserRouter>
      <Suspense fallback={<PageWrapper>loading...</PageWrapper>}>
        <Switch>
          <Route exact path="/login" component={LoginPage} />
          <Route exact path="/button" component={ButtonPage} />
          <Route exact path="/typo" component={TypoPage} />
          <Route exact path="/input" component={InputPage} />
          <Route exact path="/tag" component={TagPage} />
          <Route exact path="/modal" component={ModalPage} />
          <PrivateRoute path="/">
            <AboutPage />
          </PrivateRoute>
          <PrivateRoute path="/about">
            <AboutPage />
          </PrivateRoute>
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
