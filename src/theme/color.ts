import { darken, lighten } from "./colorManipulator";

const primaryColor = "#51cbce";
const primaryDarkenColor = darken(primaryColor, 0.4);
const primaryLightenColor = lighten(primaryColor, 0.4);
const primaryLightestColor = lighten(primaryColor, 0.8);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type colorName = any;
type colorTheme = {
  [key in colorName]: string;
};
type colorThemeCode<T> = { [key: string]: T };

const Color: colorThemeCode<colorTheme | colorName> = {
  default: {
    main: "#505B67",
    light: "#97e0e2",
    lighter: "#E8F1FB",
    dark: "#1976D2",
    backgroud: "#EFF1F2",
    border: "#B2B8C0",
  },
  primary: {
    main: primaryColor,
    light: primaryLightenColor,
    lighter: primaryLightestColor,
    dark: primaryDarkenColor,
  },
  error: {
    main: "#db302f",
    light: "#af2626",
    lighter: "#FBEAEA",
    dark: "#af2626",
    backgroud: "#FBEAEA",
    border: "#DB302F",
  },
  success: {
    main: "#43A047",
    backgroud: "#ECF5ED",
    border: "#43A047",
  },
  warning: {
    main: "#926F2A",
    backgroud: "#FDF1DA",
    border: "#F4B946",
  },
  info: {
    main: "#1976D2",
    backgroud: "#E8F1FB",
    border: "#1976D2",
  },
  text: {
    main: "#1E2227",
    light: "#838e9a",
  },
  white: "#ffffff",
};

export default Color;
