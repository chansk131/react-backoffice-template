// implementation from https://github.com/mui-org/material-ui/blob/e1457a89cc788abe59f3fb6c8038ed137a6c0520/packages/material-ui/src/styles/colorManipulator.js#L137

type Color = {
  type: string;
  values: number[];
};

/**
 * Returns a number whose value is limited to the given range.
 *
 * @param {number} value The value to be clamped
 * @param {number} min The lower boundary of the output range
 * @param {number} max The upper boundary of the output range
 * @returns {number} A number in the range [min, max]
 */
function clamp(value: number, min = 0, max = 1): number {
  if (process.env.NODE_ENV !== "production") {
    if (value < min || value > max) {
      // eslint-disable-next-line no-console
      console.error(
        `Material-UI: The value provided ${value} is out of range [${min}, ${max}].`
      );
    }
  }

  return Math.min(Math.max(min, value), max);
}

/**
 * Converts a color from CSS hex format to CSS rgb format.
 *
 * @param {string} color - Hex color, i.e. #nnn or #nnnnnn
 * @returns {string} A CSS rgb color string
 */
export function hexToRgb(color: string): string {
  const colorCode = color.substr(1);

  const re = new RegExp(`.{1,${colorCode.length >= 6 ? 2 : 1}}`, "g");
  let colors = colorCode.match(re);

  if (colors && colors[0].length === 1) {
    colors = colors.map((n) => n + n);
  }

  return colors
    ? `rgb${colors.length === 4 ? "a" : ""}(${colors
        .map((n, index) => {
          return index < 3
            ? parseInt(n, 16)
            : Math.round((parseInt(n, 16) / 255) * 1000) / 1000;
        })
        .join(", ")})`
    : "";
}

/**
 * Returns an object with the type and values of a color.
 *
 * Note: Does not support rgb % values.
 *
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @returns {Color} - A MUI color object: {type: string, values: number[]}
 */
export function decomposeColor(color: string): Color {
  if (color.charAt(0) === "#") {
    return decomposeColor(hexToRgb(color));
  }

  const marker = color.indexOf("(");
  const type = color.substring(0, marker);

  if (["rgb", "rgba", "hsl", "hsla"].indexOf(type) === -1) {
    throw new TypeError(
      `Material-UI: Unsupported ${color} color.\n" +
        "We support the following formats: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla().`
    );
  }

  const values = color.substring(marker + 1, color.length - 1).split(",");
  const floatValues = values.map((value) => parseFloat(value));

  return { type, values: floatValues };
}

/**
 * Converts a color object with type and values to a string.
 *
 * @param {Color} color - Decomposed color
 * @param {string} color.type - One of: 'rgb', 'rgba', 'hsl', 'hsla'
 * @param {array} color.values - [n,n,n] or [n,n,n,n]
 * @returns {string} A CSS color string
 */
export function recomposeColor(color: Color): string {
  const { type } = color;
  const { values } = color;
  let colorValues = ["", "", ""];

  if (type.indexOf("rgb") !== -1) {
    // Only convert the first 3 values to int (i.e. not alpha)
    colorValues = values.map((n, i) => (i < 3 ? Math.round(n) : n).toString());
  } else if (type.indexOf("hsl") !== -1) {
    colorValues[1] = `${values[1]}%`;
    colorValues[2] = `${values[2]}%`;
  }

  return `${type}(${colorValues.join(", ")})`;
}

/**
 * Darkens a color.
 *
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @param {number} coefficient - multiplier in the range 0 - 1
 * @returns {string} A CSS color string. Hex input values are returned as rgb
 */
export function darken(color: string, coefficient: number): string {
  const decomposedColor = decomposeColor(color);
  const clampedCoefficient = clamp(coefficient);

  if (decomposedColor.type.indexOf("hsl") !== -1) {
    decomposedColor.values[2] *= 1 - clampedCoefficient;
  } else if (decomposedColor.type.indexOf("rgb") !== -1) {
    for (let i = 0; i < 3; i += 1) {
      decomposedColor.values[i] *= 1 - clampedCoefficient;
    }
  }
  return recomposeColor(decomposedColor);
}

/**
 * Lightens a color.
 *
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @param {number} coefficient - multiplier in the range 0 - 1
 * @returns {string} A CSS color string. Hex input values are returned as rgb
 */
export function lighten(color: string, coefficient: number): string {
  const decomposedColor = decomposeColor(color);
  const clampedCoefficient = clamp(coefficient);

  if (decomposedColor.type.indexOf("hsl") !== -1) {
    decomposedColor.values[2] +=
      (100 - decomposedColor.values[2]) * clampedCoefficient;
  } else if (decomposedColor.type.indexOf("rgb") !== -1) {
    for (let i = 0; i < 3; i += 1) {
      decomposedColor.values[i] +=
        (255 - decomposedColor.values[i]) * clampedCoefficient;
    }
  }

  return recomposeColor(decomposedColor);
}
